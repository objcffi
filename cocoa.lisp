(in-package :objcffi)

(cffi:defcstruct NSRange (location :unsigned-int) (length :unsigned-int))
(cffi:defcstruct NSPoint (x :float) (y :float)) ;; NO
(cffi:defcstruct NSSize (width :float) (height :float))
(cffi:defcstruct NSRect (origin nspoint) (size nssize))

(name-objc-struct "_NSRange" 'NSRange)
(name-objc-struct "_NSPoint" 'NSPoint)
(name-objc-struct "_NSSize" 'NSSize)
(name-objc-struct "_NSRect" 'NSRect)

(defun NSMakeRange (location length)
  (let ((range (cffi:foreign-alloc 'NSRange)))
    (setf (cffi:foreign-slot-value range 'NSRange 'location) location
	  (cffi:foreign-slot-value range 'NSRange 'length) length)
    range))

(defun NSMakePoint (x y)
  (let ((point (cffi:foreign-alloc 'NSPoint)))
    (setf (cffi:foreign-slot-value point 'NSPoint 'x) x
	  (cffi:foreign-slot-value point 'NSPoint 'y) y)
    point))

(defun NSMakeSize (width height)
  (let ((size (cffi:foreign-alloc 'NSSize)))
    (setf (cffi:foreign-slot-value size 'NSSize 'width) width
	  (cffi:foreign-slot-value size 'NSSize 'height) height)
    size))

(defun NSMakeRect (x y width height)
  (let ((rect (cffi:foreign-alloc 'nsrect)))
    (setf (cffi:foreign-slot-value (cffi:foreign-slot-value rect 'nsrect 'origin) 'nspoint 'x) x
	  (cffi:foreign-slot-value (cffi:foreign-slot-value rect 'nsrect 'origin) 'nspoint 'y) y
	  (cffi:foreign-slot-value (cffi:foreign-slot-value rect 'nsrect 'size) 'nssize 'width)  width
	  (cffi:foreign-slot-value (cffi:foreign-slot-value rect 'nsrect 'size) 'nssize 'height) height)
    rect))
