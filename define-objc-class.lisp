(in-package :objcffi)

(defun find-root-class (class)
  (let ((parent (cffi:foreign-slot-value class 'objc_class 'super_class)))
    (if (cffi:null-pointer-p parent) class
	(find-root-class parent))))

(defun null-method-list ()
  (cffi:foreign-alloc
   :pointer :initial-element (cffi:make-pointer #xffffffff))) ;; TODO calculate this number
;;  (let ((method-list (cffi:foreign-alloc 'objc_method_list)))
;;    (setf (cffi:foreign-slot-value method-list 'objc_method_list 'obsolete) (cffi:null-pointer)
;;	  (cffi:foreign-slot-value method-list 'objc_method_list 'method_count) 0
;;	  (cffi:foreign-slot-value method-list 'objc_method_list 'method_list) (cffi:null-pointer))
;;    method-list))

(defmacro with-named-foreign-slots ((object type) &rest body)
  ;; possibly a useful addition to CFFI?
  (let* ((slots (cffi:foreign-slot-names type))
	 (named-slots (mapcar
		       (lambda (slot)
			 (intern
			  (concatenate 'string (string object) "-" (string slot))))
		       slots)))
    `(symbol-macrolet
	 ,(mapcar (lambda (slot slot-name)
		    `(,slot-name (cffi:foreign-slot-value ,object ',type ',slot)))
		  slots named-slots) ,@body)))

(defmacro define-objc-class (name superclass ivar-list &body body)
  `(progn
     (%define-objc-class ,name ,superclass
			 ',(mapcar (lambda (ivar-spec)
				     (list (first ivar-spec) (second ivar-spec)))
				   ivar-list))
     (symbol-macrolet ,(mapcan (lambda (ivar-spec)
				 (when (= (length ivar-spec) 3)
				   (list `(,(third ivar-spec) (ivar self ,(second ivar-spec)))))) ivar-list)
       ,@body)))

(defun-with-types %define-objc-class (class-name (superclass :objc-class) ivars)
  ;; should be able to say this symbol is exported with the package here
  ;; ivars is a list of (type objective-c-name-string)
  ;;
  ;; CANNOT DECLARE ONES WITH STRUCTS AS IVARS YET
  ;;
  (let (newclass metaclass rootclass)
    
    ;; The superclass must exist
    (when (cffi:null-pointer-p superclass)
      (error "You cannot make a subclass of a null pointer"))
    
    ;; You cannot redefine a class, check if its got the same ivars as the current definition
    (let ((current-definition (%objc_lookUpClass class-name)))
      (unless (cffi:null-pointer-p current-definition)
	(if (every (lambda (ivar-spec defined-ivar)
		     (and (eq (first ivar-spec) (first defined-ivar))
			  (string= (second ivar-spec) (second defined-ivar))))
		   ivars (%list-ivars current-definition))
	    (return-from %define-objc-class current-definition)
	    (error "An incompatable definition of class \"~a\" already exists" class-name))))
    
    ;; We must find the root class
    (setq rootclass (find-root-class superclass))
    
    (setq newclass (cffi:foreign-alloc 'objc_class)
	  metaclass (cffi:foreign-alloc 'objc_class))
    
    (let* ((instance-size (cffi:foreign-slot-value superclass 'objc_class 'instance_size))
	   (offset instance-size)
	   (ivars-ptr (cffi:foreign-alloc
		       :char
		       :count (+ (cffi:foreign-type-size 'objc_ivar_list)
				 (* (1- (length ivars)) (cffi:foreign-type-size 'objc_ivar)))))
	   (ivar-ptr (cffi:inc-pointer
		      ivars-ptr
		      (cffi:foreign-slot-offset 'objc_ivar_list 'ivar_list))))
      (setf (cffi:foreign-slot-value ivars-ptr 'objc_ivar_list 'ivar_count) (length ivars))
      (dolist (ivar-spec ivars)
	(let ((ivar (cffi:mem-ref ivar-ptr 'objc_ivar)))
	  (multiple-value-bind (type-string type-size) (encode-type (first ivar-spec))
	    (if (null type-string)
		(error "Ivar of incorrect/unsupported type: \"~a\"" (first ivar-spec)))
	  (setf (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_name)
		(second ivar-spec))
	  (setf (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_type)
		type-string)
	  (setf (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_offset)
		offset)
	  (incf offset type-size)
	  (cffi:incf-pointer ivar-ptr (cffi:foreign-type-size 'objc_ivar)))))
      
      (with-named-foreign-slots (newclass objc_class)
        (setf newclass-isa           metaclass
	      newclass-super_class   superclass
	      newclass-name          class-name
	      newclass-version       0
	      newclass-info          CLS_CLASS
	      newclass-instance_size offset ;; this is instance-size + ivar-length
	      newclass-ivars         ivars-ptr
	      newclass-methodLists   (null-method-list);;(cffi:foreign-alloc 'objc_method_list)
	      newclass-cache         (cffi:null-pointer)
	      newclass-protocols     (cffi:null-pointer))))
    
    (with-named-foreign-slots (metaclass objc_class)
      (setf metaclass-isa           (cffi:foreign-slot-value rootclass 'objc_class 'isa)
	    metaclass-super_class   (cffi:foreign-slot-value superclass 'objc_class 'isa)
	    metaclass-name          (cffi:foreign-slot-value newclass 'objc_class 'name)
	    metaclass-version       0
	    metaclass-info          CLS_META
	    metaclass-instance_size (cffi:foreign-slot-value
				     (cffi:foreign-slot-value superclass 'objc_class 'isa)
				     'objc_class 'instance_size)
	    metaclass-ivars         (cffi:null-pointer)
	    metaclass-methodLists   (null-method-list);;(cffi:foreign-alloc 'objc_method_list)
	    metaclass-cache         (cffi:null-pointer)
	    metaclass-protocols     (cffi:null-pointer)))
    
    (%objc_addClass newclass)
    
    newclass))
