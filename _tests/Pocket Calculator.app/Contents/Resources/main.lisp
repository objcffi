(require :cffi)
(require :objcffi)
;;(load "/Users/ed/bin/slime/swank-loader.lisp")

(cffi:load-foreign-library '(:framework "Cocoa"))

(defparameter *memory* nil)
(defparameter *op* nil)

(objcffi:define-objc-class "Calculator" "NSObject"
  ((objc:id "display")))

(objcffi:define-objc-method "Calculator" - (:void "numericButtonPressed:" objc:id sender)
  (objc:send (objc:ivar objc::self "display") "setStringValue:"
	     (objc:send
	      (objc:send (objc:ivar objc::self "display") "stringValue")
	      "stringByAppendingString:"
	      (objc:send sender "title"))))

(objcffi:define-objc-method "Calculator" - (:void "row1function:" objc:id sender)
    (if *memory*
	(let ((display-value (objc:send (objc:ivar objc::self "display") "floatValue")))
	  (objc:send (objc:ivar objc::self "display") "setFloatValue:"
		     (funcall *op* *memory* display-value))
	  (setf *memory* display-value))
      (progn (setf *memory* (objc:send (objc:ivar objc::self "display") "floatValue")
		   *op* (nth (objc:send sender "selectedColumn")
			     (list #'+ #'- #'* #'/)))
	     (objc:send (objc:ivar objc::self "display") "setStringValue:"
			(objc:send "NSString" "stringWithUTF8String:" "")))))

(objcffi:define-objc-method "Calculator" - (:void "row2function:" objc:id sender)
  (let ((op-index (objc:send sender "selectedColumn")))
    (if (= op-index 1)
	(if *memory*
	    (let ((display-value (objc:send (objc:ivar objc::self "display") "floatValue")))
	      (objc:send (objc:ivar objc::self "display") "setFloatValue:"
			 (expt *memory* display-value))
	      (setf *memory* display-value))
	  (progn (setf *memory* (objc:send (objc:ivar objc::self "display") "floatValue")
		       *op* #'expt)
		 (objc:send (objc:ivar objc::self "display") "setStringValue:"
			    (objc:send "NSString" "stringWithUTF8String:" ""))))
	(objc:send (objc:ivar objc::self "display") "setFloatValue:"
		   (funcall (nth op-index (list #'sqrt #'identity #'log #'exp))
			    (objc:send (objc:ivar objc::self "display") "floatValue"))))))

(objcffi:define-objc-method "Calculator" - (:void "row3function:" objc:id sender)
  (objc:send (objc:ivar objc::self "display") "setFloatValue:"
		 (funcall (nth (objc:send sender "selectedColumn")
			       (list #'sin #'cos #'tan #'identity))
			  (objc:send (objc:ivar objc::self "display") "floatValue"))))

(objcffi:define-objc-method "Calculator" - (:void "row4function:" objc:id sender)
  (objc:send (objc:ivar objc::self "display") "setFloatValue:"
		 (funcall (nth (objc:send sender "selectedColumn")
			       (list #'asin #'acos #'atan #'identity))
			  (objc:send (objc:ivar objc::self "display") "floatValue"))))

(objcffi:define-objc-method "Calculator" - (:void "row5function:" objc:id sender)
  (objc:send (objc:ivar objc::self "display") "setFloatValue:"
	     (coerce (nth (objc:send sender "selectedColumn")
			  (list pi (exp 1) (/ (1+ (sqrt 5)) 2) 0.00001))
		     'float)))

(objcffi:define-objc-method "Calculator" - (:void "equals:" objc:id sender)
  (let ((display-value (objc:send (objc:ivar objc::self "display") "floatValue")))
	  (objc:send (objc:ivar objc::self "display") "setFloatValue:"
		     (funcall *op* *memory* display-value))
	  (setf *memory* display-value)))

(objcffi:define-objc-method "Calculator" - (:void "erase:" objc:id sender)
  (setf *memory* nil)
  (objc:send (objc:ivar objc::self "display") "setStringValue:"
			(objc:send "NSString" "stringWithUTF8String:" "")))


;;(swank:create-server)

(cffi:foreign-funcall "NSApplicationMain" :int 0 :pointer (cffi:null-pointer))
