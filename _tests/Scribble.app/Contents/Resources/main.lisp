(require :cffi)
(require :objcffi)
(import 'objcffi::self)
(load "/Users/ed/bin/slime/swank-loader.lisp")
(cffi:load-foreign-library '(:framework "Cocoa"))

;;(defparameter *image* nil) ;; should make this a class ivar

(objc:define-objc-class "ScribbleView" "NSView" ((objc:id "image")
						 (objc:id "colorWell")))

(objc:define-objc-method "ScribbleView" - (:void "awakeFromNib")
  (format t "Awaked~%")
  (setf (objc:ivar self "image")
	(objc:send (objc:send "NSImage" "alloc") "initWithSize:" (objc:NSMakeSize 320.0 240.0)))
  (objc:send (objc:ivar self "image") "lockFocus")
  (objc:send (objc:send "NSColor" "whiteColor") "set")
  (cffi:foreign-funcall "NSRectFill" objc:NSRect (objc:NSMakeRect 0.0 0.0 320.0 240.0))
  (objc:send (objc:ivar self "image") "unlockFocus"))

(objc:define-objc-method "ScribbleView" - (:void "drawRect:" :float x :float y :float width :float height)
  (objc:send (objc:ivar self "image") "compositeToPoint:operation:" (objc:NSMakePoint 0.0 0.0) 2))

(objc:define-objc-method "ScribbleView" - (:void "plot::" :float x :float y)
  (objc:send (objc:ivar self "image") "lockFocus")
  ;;(objc:send (objc:send "NSColor" "blackColor") "set")
  (objc:send (objc:send (objc:ivar self "colorWell") "color") "set")
  (objc:send (objc:send "NSBezierPath" "bezierPathWithOvalInRect:" (objc:NSMakeRect (- x 3.0) (- y 3.0) 6.0 6.0)) "fill")
  (objc:send (objc:ivar self "image") "unlockFocus"))

(objc:define-objc-method "ScribbleView" - (:void "mouseDragged:" objc:id event)
  (let* ((location (objc:send self "convertPoint:fromView:" (objc:send event "locationInWindow") (cffi:null-pointer)))
         (x (cffi:foreign-slot-value location 'objc:NSPoint 'objc:x))
	 (y (cffi:foreign-slot-value location 'objc:NSPoint 'objc:y)))
    (objc:send self "plot::" x y)
    (objc:send self "setNeedsDisplay:" 1)))



(objc:define-objc-class "MirrorScribbleView" "ScribbleView" ())

(objc:define-objc-method "MirrorScribbleView" - (:void "mouseDragged:" objc:id event)
  (let* ((location (objc:send self "convertPoint:fromView:" (objc:send event "locationInWindow") (cffi:null-pointer)))
         (x (cffi:foreign-slot-value location 'objc:NSPoint 'objc:x))
	 (y (cffi:foreign-slot-value location 'objc:NSPoint 'objc:y)))
    (objc:send self "plot::" x y)
    (objc:send self "plot::" (- 320.0 x) y)
    (objc:send self "plot::" x (- 240.0 y))
    (objc:send self "plot::" (- 320.0 x) (- 240.0 y))
    (objc:send self "setNeedsDisplay:" 1)))



(swank:create-server :port 6666)
(cffi:foreign-funcall "NSApplicationMain" :int 0 :pointer (cffi:null-pointer))
