(defpackage :pokemon
  (:use :common-lisp)
  (:export :pokedex-report :pokemon-report :pokemon-number))
(in-package :pokemon)

(defparameter *pokemon*
  '(("Bulbasaur"
     ("Grass" "Poison")
     (2 4)
     15
     ("Ivysaur" 16)
     "A strange seed was planted on its back at birth. The plant sprouts and grows with this Pokemon."
     (("Tackle" "Normal")
      ("Growl" "Normal")
      ("Leech Seed" "Grass" 7)
      ("Vine Whip" "Grass" 13)
      ("Poison Powder" "Poison" 20)
      ("Razor Leaf" "Grass" 27)
      ("Growth" "Normal" 34)
      ("Sleep Powder" "Grass" 41)
      ("Solar Beam" "Grass" 48)))
    ("Ivysaur"
     ("Grass" "Poison")
     (3 3)
     29
     ("Venusaur" 32)
     "When the bulb on its back grows large, it appears to lose the speed to stand on its hind legs."
     (("Tackle" "Normal")
      ("Growl" "Normal")
      ("Leech Seed" "Grass")
      ("Vine Whip" "Grass")
      ("Poison Powder" "Poison" 22)
      ("Razor Leaf" "Grass" 30)
      ("Growth" "Normal" 38)
      ("Sleep Powder" "Grass" 46)
      ("Solar Beam" "Grass" 54)))
    ("Venusaur"
     ("Grass" "Poison")
     (6 7)
     221
     ()
     "The plant blooms when it is absorbing solar energy. It stays on the move to seek sunlight."
     (("Tackle" "Normal")
      ("Growl" "Normal")
      ("Leech Seed" "Grass")
      ("Vine Whip" "Grass")
      ("Poison Powder" "Poison")
      ("Razor Leaf" "Grass")
      ("Growth" "Normal" 43)
      ("Sleep Powder" "Grass" 55)
      ("Solar Beam" "Grass" 65)))
    ("Charmander"
     ("Fire")
     (2 0)
     19
     ("Charmeleon" 16)
     "Obviously prefers hot places. When it rains, steam is said to spout from the tip of its tail."
     (("Scratch" "Normal")
      ("Growl" "Normal")
      ("Ember" "Fire" 9)
      ("Leer" "Normal" 15)
      ("Rage" "Normal" 22)
      ("Slash" "Normal" 30)
      ("Flamethrower" "Fire" 38)
      ("Fire Spin" "Fire" 46)))
    ("Charmeleon"
     ("Fire")
     (3 7)
     42
     ("Charizard" 36)
     "When it swings its burning tail, it elevates the temperature to unbearably high levels."
     (("Scratch" "Normal")
      ("Growl" "Normal")
      ("Ember" "Fire")
      ("Leer" "Normal")
      ("Rage" "Normal" 24)
      ("Slash" "Normal" 33)
      ("Flamethrower" "Fire" 42)
      ("Fire Spin" "Fire" 56)))
    ("Charizard"
     ("Fire")
     (5 7)
     200
     ()
     "Spits fire that is hot enough to melt boulders. Known to cause forest fires unintentionally."
     (("Scratch" "Normal")
      ("Growl" "Normal")
      ("Ember" "Fire")
      ("Leer" "Normal")
      ("Rage" "Normal")
      ("Slash" "Normal" 36)
      ("Flamethrower" "Fire" 46)
      ("Fire Spin" "Fire" 55)))
    ("Squirtle"
     ("Water")
     (1 8)
     20
     ("Wartortle" 16)
     "After birth, its back swells and hardens into a shell. Powerfully sprays foam from its mouth."
     (("Tackle" "Normal")
      ("Tail Whip" "Normal")
      ("Bubble" "Water" 8)
      ("Water Gun" "Water" 15)
      ("Bite" "Normal" 22)
      ("Withdraw" "Water" 28)
      ("Skull Bash" "Normal" 35)
      ("Hydro Pump" "Water" 42)))
    ("Wartortle"
     ("Water")
     (3 3)
     50
     ("Blastoise" 36)
     "Often hides in water to stalk unwary prey. For swimming fast, it moves its ears to maintain balance."
     (("Tackle" "Normal")
      ("Tail Whip" "Normal")
      ("Bubble" "Water")
      ("Water Gun" "Water")
      ("Bite" "Normal" 24)
      ("Withdraw" "Water" 31)
      ("Skull Bash" "Normal" 39)
      ("Hydro Pump" "Water" 47)))
    ("Blastoise"
     ("Water")
     (5 3)
     189
     ()
     "A brutal Pokemon with pressurized water jets on its shell. They are used for high speed tackles."
     (("Tackle" "Normal")
      ("Tail Whip" "Normal")
      ("Bubble" "Water")
      ("Water Gun" "Water")
      ("Bite" "Normal")
      ("Withdraw" "Water")
      ("Skull Bash" "Normal" 42)
      ("Hydro Pump" "Water" 52)))
    ("Caterpie"
     ("Bug")
     (1 0)
     6
     ("Metapod" 7)
     "Its short feet are tipped with suction pads that enable it to tirelessly climb slopes and walls."
     (("Tackle" "Normal")
      ("String Shot" "Bug")))
    ("Metapod"
     ("Bug")
     (2 4)
     22
     ("Butterfree" 10)
     "This Pokemon is vulnerable to attack while its shell is soft, exposing its weak and tender body."
     (("Tackle" "Normal")
      ("String Shot" "Bug")
      ("Harden" "Normal")))
    ("Butterfree"
     ("Bug" "Flying")
     (3 7)
     71
     ()
     "In battle, it flaps its wings at high speed to release highly toxic dust into the air."
     (("Tackle" "Normal")
      ("String Shot" "Bug")
      ("Harden" "Normal")
      ("Confusion" "Psychic" 10)
      ("Poison Powder" "Poison" 13)
      ("Stun Spore" "Grass" 14)
      ("Sleep Powder" "Grass" 15)
      ("Supersonic" "Normal" 18)
      ("Whirlwind" "Normal" 23)
      ("Psybeam" "Psychic" 34)))
    ("Weedle"
     ("Bug" "Poison")
     (1 0)
     7
     ("Kakuna" 7)
     "Often found in forests, eating leaves. It has a sharp venomous stinger on its head."
     (("Poison Sting" "Poison")
      ("String Shot" "Bug")))
    ("Kakuna"
     ("Bug" "Poison")
     (2 0)
     22
     ("Beedrill" 10)
     "Almost incapable of moving, this Pokemon can only harden its shell to protect itself from predators."
     (("Poison Sting" "Poison")
      ("String Shot" "Bug")
      ("Harden" "Normal")))
    ("Beedrill"
     ("Bug" "Poison")
     (3 3)
     65
     ()
     "Flies at high speed and attacks using its large venomous stingers on its forelegs and tail."
     (("Poison Sting" "Poison")
      ("String Shot" "Bug")
      ("Harden" "Normal")
      ("Fury Attack" "Normal" 12)
      ("Focus Energy" "Normal" 16)
      ("Twineedle" "Bug" 20)
      ("Rage" "Normal" 25)
      ("Pin Missile" "Bug" 30)
      ("Agility" "Psychic" 35)))
    ("Pidgey"
     ("Normal" "Flying")
     (1 0)
     4
     ("Pidgeotto" 18)
     "A common sight in forests and woods. It flaps its wings at ground level to kick up blinding sand."
     (("Gust" "Normal")
      ("Sand-Attack" "Normal" 5)
      ("Quick Attack" "Normal" 12)
      ("Whirlwind" "Normal" 19)
      ("Wing Attack" "Flying" 28)
      ("Agility" "Psychic" 36)
      ("Mirror Move" "Flying" 44)))
    ("Pidgeotto"
     ("Normal" "Flying")
     (3 7)
     66
     ("Pidgeot" 36)
     "Very protective of its sprawling territorial area, this Pokemon will fiercely peck at any intruder."
     (("Gust" "Normal")
      ("Sand-Attack" "Normal")
      ("Quick Attack" "Normal")
      ("Whirlwind" "Normal" 21)
      ("Wing Attack" "Flying" 31)
      ("Agility" "Psychic" 40)
      ("Mirror Move" "Flying" 49)))
    ("Pidgeot"
     ("Normal" "Flying")
     (4 11)
     87
     ()
     "When hunting, it skims the surface of water at high speed to pick off unwary prey such as Magikarp."
     (("Gust" "Normal")
      ("Sand-Attack" "Normal")
      ("Quick Attack" "Normal")
      ("Whirlwind" "Normal")
      ("Wing Attack" "Flying")
      ("Agility" "Psychic" 44)
      ("Mirror Move" "Flying" 54)))
    ("Rattata"
     ("Normal")
     (1 0)
     8
     ("Raticate" 20)
     "Bites anything when it attacks. Small and very quick, it is a common sight in many places."
     (("Tackle" "Normal")
      ("Tail Whip" "Normal")
      ("Quick Attack" "Normal" 9)
      ("Hyper Fang" "Normal" 14)
      ("Focus Energy" "Normal" 23)
      ("Super Fang" "Normal" 34)))
    ("Raticate"
     ("Normal")
     (2 4)
     41
     ()
     "It uses its whiskers to maintain balance. It apparently slows down if they are cut off."
     (("Tackle" "Normal")
      ("Tail Whip" "Normal")
      ("Quick Attack" "Normal")
      ("Hyper Fang" "Normal")
      ("Focus Energy" "Normal" 27)
      ("Super Fang" "Normal" 41)))
    ("Spearow"
     ("Normal" "Flying")
     (1 0)
     4
     ("Fearow" 20)
     "Eats bugs in grassy area. It has to flap its short wings at high speed to stay airborne."
     (("Peck" "Flying")
      ("Growl" "Normal")
      ("Leer" "Normal" 9)
      ("Fury Attack" "Normal" 15)
      ("Mirror Move" "Flying" 22)
      ("Drill Peck" "Flying" 29)
      ("Agility" "Psychic" 36)))
    ("Fearow"
     ("Normal" "Flying")
     (3 11)
     84
     ()
     "With its huge and magnificent wings, it can keep aloft without ever having to land for rest."
     (("Peck" "Flying")
      ("Growl" "Normal")
      ("Leer" "Normal")
      ("Fury Attack" "Normal")
      ("Mirror Move" "Flying" 25)
      ("Drill Peck" "Flying" 34)
      ("Agility" "Psychic" 43)))
    ("Ekans"
     ("Poison")
     (6 7)
     15
     ("Arbok" 22)
     "Moves silently and stealthily. Eats the eggs of birds, such as Pidgey and Spearow, whole."
     (("Wrap" "Normal")
      ("Leer" "Normal")
      ("Poison Sting" "Poison" 10)
      ("Bite" "Normal" 17)
      ("Glare" "Normal" 24)
      ("Screech" "Normal" 31)
      ("Acid" "Poison" 38)))
    ("Arbok"
     ("Poison")
     (11 6)
     143
     ()
     "It is rumored that the ferocious warning markings on its belly differ from area to area."
     (("Wrap" "Normal")
      ("Leer" "Normal")
      ("Poison Sting" "Poison")
      ("Bite" "Normal")
      ("Glare" "Normal" 27)
      ("Screech" "Normal" 36)
      ("Acid" "Poison" 47)))
    ("Pikachu"
     ("Electric")
     (1 4)
     13
     ("Raichu" "Thunder Stone")
     "When several of these Pokemon gather, their electricity could build and cause lightning storms."
     (("Thundershock" "Electric")
      ("Growl" "Normal")
      ("Thunder Wave" "Electric" 9)
      ("Quick Attack" "Normal" 16)
      ("Swift" "Normal" 26)
      ("Agility" "Psychic" 33)
      ("Thunder" "Electric" 43)))
    ("Raichu"
     ("Electric")
     (2 7)
     66
     ()
     "Its long tail serves as a ground to protect itself from its own high voltage power."
     (("Thundershock" "Electric")
      ("Growl" "Normal")
      ("Thunder Wave" "Electric")))
    ("Sandshrew"
     ("Ground")
     (2 0)
     26
     ("Sandslash" 22)
     "Burrows deep underground in arid locations far from water. It only emerges to hunt for food."
     (("Scratch" "Normal")
      ("Sand-Attack" "Normal" 10)
      ("Slash" "Normal" 17)
      ("Poison Sting" "Poison" 24)
      ("Swift" "Normal" 31)
      ("Fury Swipes" "Normal" 38)))
    ("Sandslash"
     ("Ground")
     (3 3)
     65
     ()
     "Curls up into a ball when threatened. It can roll while curled up to attack or escape."
     (("Scratch" "Normal")
      ("Sand-Attack" "Normal")
      ("Slash" "Normal")
      ("Poison Sting" "Poison" 27)
      ("Swift" "Normal" 36)
      ("Fury Swipes" "Normal" 47)))
    ("Nidoran F"
     ("Poison")
     (1 4)
     15
     ("Nidorina" 16)
     "Although small, its venomous barbs render this Pokemon dangerous. The female has smaller horns."
     (("Growl" "Normal")
      ("Tackle" "Normal")
      ("Scratch" "Normal" 8)
      ("Poison Sting" "Poison" 14)
      ("Tail Whip" "Normal" 21)
      ("Bite" "Normal" 29)
      ("Fury Swipes" "Normal" 36)
      ("Double Kick" "Fighting" 43)))
    ("Nidorina"
     ("Poison")
     (2 7)
     44
     ("Nidoqueen" "Moon Stone")
     "The female's horn develops slowly. Prefers physical attacks such as clawing and biting."
     (("Growl" "Normal")
      ("Tackle" "Normal")
      ("Scratch" "Normal")
      ("Poison Sting" "Poison")
      ("Tail Whip" "Normal" 23)
      ("Bite" "Normal" 32)
      ("Fury Swipes" "Normal" 41)
      ("Double Kick" "Fighting" 50)))
    ("Nidoqueen"
     ("Poison" "Ground")
     (4 3)
     132
     ()
     "Its hard scales provide strong protection. It uses its hefty bulk to execute powerful moves."
     (("Growl" "Normal")
      ("Tackle" "Normal")
      ("Scratch" "Normal")
      ("Poison Sting" "Poison")
      ("Body Slam" "Normal" 23)))
    ("Nidoran M"
     ("Poison")
     (1 8)
     20
     ("Nidorino" 16)
     "Stiffens its ears to sense danger. The larger its horns, the more powerful its secreted venom."
     (("Leer" "Normal")
      ("Tackle" "Normal")
      ("Horn Attack" "Normal" 8)
      ("Poison Sting" "Poison" 14)
      ("Focus Energy" "Normal" 21)
      ("Fury Attack" "Normal" 29)
      ("Horn Drill" "Normal" 36)
      ("Double Kick" "Fighting" 43)))
    ("Nidorino"
     ("Poison")
     (2 11)
     43
     ("Nidoking" "Moon Stone")
     "An aggressive Pokemon that is quick to attack. The horn on its head secretes a powerful venom."
     (("Leer" "Normal")
      ("Tackle" "Normal")
      ("Horn Attack" "Normal")
      ("Poison Sting" "Poison")
      ("Focus Energy" "Normal" 23)
      ("Fury Attack" "Normal" 32)
      ("Horn Drill" "Normal" 41)
      ("Double Kick" "Fighting" 50)))
    ("Nidoking"
     ("Poison" "Ground")
     (4 7)
     137
     ()
     "It uses its powerful tail in battle to smash, constrict, then break the prey's bones."
     (("Leer" "Normal")
      ("Tackle" "Normal")
      ("Horn Attack" "Normal")
      ("Poison Sting" "Poison")
      ("Thrash" "Normal" 23)))
    ("Clefairy"
     ("Normal")
     (2 0)
     17
     ("Clefable" "Moon Stone")
     "Its magical and cute appeal has many admirers. It is rare and found only in certain areas."
     (("Pound" "Normal")
      ("Growl" "Normal")
      ("Sing" "Normal" 13)
      ("Doubleslap" "Normal" 18)
      ("Minimize" "Normal" 24)
      ("Metronome" "Normal" 31)
      ("Defense Curl" "Normal" 39)
      ("Light Screen" "Psychic" 48)))
    ("Clefable"
     ("Normal")
     (4 3)
     88
     ()
     "A timid fairy Pokemon that is rarely seen. It will run and hide the moment it senses people."
     (("Pound" "Normal")
      ("Growl" "Normal")
      ("Sing" "Normal" 13)
      ("Doubleslap" "Normal")
      ("Minimize" "Normal")
      ("Metronome" "Normal")
      ("Defense Curl" "Normal")
      ("Light Screen" "Psychic")))
    ("Vulpix"
     ("Fire")
     (2 0)
     22
     ("Ninetales" "Fire Stone")
     "At the time of its birth, it has just one tail. The tail splits from its tip as it grows older."
     (("Ember" "Fire")
      ("Tail Whip" "Normal")
      ("Quick Attack" "Normal" 16)
      ("Roar" "Normal" 21)
      ("Confuse Ray" "Ghost" 28)
      ("Flamethrower" "Fire" 35)
      ("Fire Spin" "Fire" 42)))
    ("Ninetales"
     ("Fire")
     (3 7)
     44
     ()
     "Very smart and very vengeful. Grabbing one of its many tails could result in a 1000-year curse."
     ())
    ("Jigglypuff"
     ("Normal")
     (1 8)
     12
     ("Wigglytuff" "Moon Stone")
     "When its huge eyes light up, it sings a mysteriously soothing melody that lulls its enemies to sleep."
     (("Sing" "Normal")
      ("Pound" "Normal" 9)
      ("Disable" "Normal" 14)
      ("Defense Curl" "Normal" 19)
      ("Doubleslap" "Normal" 24)
      ("Rest" "Psychic" 29)
      ("Body Slam" "Normal" 34)
      ("Double-Edge" "Normal" 39)))
    ("Wigglytuff"
     ("Normal")
     (3 3)
     26
     ()
     "The body is soft and rubbery. When angered, it will suck in air and inflate itself to an enormous size."
     ())
    ("Zubat"
     ("Poison" "Flying")
     (2 7)
     17
     ("Golbat" 22)
     "Forms colonies in perpetually dark places. Uses ultrasonic waves to identify and approach targets."
     (("Leech Life" "Bug")
      ("Supersonic" "Normal" 10)
      ("Bite" "Normal" 15)
      ("Confuse Ray" "Ghost" 21)
      ("Wing Attack" "Flying" 28)
      ("Haze" "Ice" 36)))
    ("Golbat"
     ("Poison" "Flying")
     (5 3)
     121
     ()
     "Once it strikes, it will not stop draining energy from the victim even if it gets too heavy to fly."
     (("Leech Life" "Bug")
      ("Supersonic" "Normal")
      ("Bite" "Normal")
      ("Confuse Ray" "Ghost")
      ("Wing Attack" "Flying" 32)
      ("Haze" "Ice" 43)))
    ("Oddish"
     ("Grass" "Poison")
     (1 8)
     12
     ("Gloom" 21)
     "During the day, it keeps its face buried in the ground. At night, it wanders around sowing its seeds."
     (("Absorb" "Grass")
      ("Poison Powder" "Poison" 15)
      ("Stun Spore" "Grass" 17)
      ("Sleep Powder" "Grass" 19)
      ("Acid" "Poison" 24)))
    ("Gloom"
     ("Grass" "Poison")
     (2 7)
     19
     ("Vileplume" "Leaf Stone")
     "The fluid that oozes from its mouth isn't drool. It is a nectar that is used to attract prey."
     (("Absorb" "Grass")
      ("Poison Powder" "Poison")
      ("Stun Spore" "Grass")
      ("Sleep Powder" "Grass")
      ("Acid" "Poison" 28)))
    ("Vileplume"
     ("Grass" "Poison")
     (3 11)
     41
     ()
     "The larger its petals, the more toxic pollen it contains. Its big head is heavy and hard to hold up."
     ())
    ("Paras"
     ("Bug" "Grass")
     (1 0)
     12
     ("Parasect" 24)
     "Burrows to suck tree roots. The mushrooms on its back grow by drawing nutrients from the bug host."
     (("Scratch" "Normal")
      ("Stun Spore" "Grass" 13)
      ("Leech Life" "Grass" 20)
      ("Spore" "Bug" 27)
      ("Slash" "Normal" 34)
      ("Growth" "Normal" 41)))
    ("Parasect"
     ("Bug" "Grass")
     (3 3)
     65
     ()
     "A host-parasite pair in which the parasite mushroom has taken over the host bug. Prefers damp places."
     (("Scratch" "Normal")
      ("Stun Spore" "Grass")
      ("Leech Life" "Grass")
      ("Spore" "Bug" 30)
      ("Slash" "Normal" 39)
      ("Growth" "Normal" 48)))
    ("Venonat"
     ("Bug" "Poison")
     (3 3)
     66
     ("Venomoth" 31)
     "Lives in the shadows of tall trees where it eats insects. It is attracted by light at night."
     (("Tackle" "Normal")
      ("Disable" "Normal")
      ("Poison Powder" "Poison" 24)
      ("Leech Life" "Grass" 27)
      ("Stun Spore" "Grass" 30)
      ("Psybeam" "Psychic" 35)
      ("Sleep Powder" "Grass" 38)
      ("Psychic" "Psychic" 43)))
    ("Venomoth"
     ("Bug" "Poison")
     (4 11)
     28
     ()
     "The dust-like scales covering its wings are color coded to indicate the kinds of poison it has."
     (("Tackle" "Normal")
      ("Disable" "Normal")
      ("Poison Powder" "Poison")
      ("Leech Life" "Grass")
      ("Stun Spore" "Grass")
      ("Psybeam" "Psychic" 38)
      ("Sleep Powder" "Grass" 43)
      ("Psychic" "Psychic" 50)))
    ("Diglett"
     ("Ground")
     (0 8)
     2
     ("Dugtrio" 26)
     "Lives about one yard underground where it feeds on plant roots. It sometimes appears above ground."
     (("Scratch" "Normal")
      ("Growl" "Normal")
      ("Dig" "Ground" 19)
      ("Sand-Attack" "Normal" 24)
      ("Slash" "Normal" 31)
      ("Earthquake" "Ground" 40)))
    ("Dugtrio"
     ("Ground")
     (2 4)
     73
     ()
     "A team of Diglett triplets. It triggers huge earthquakes by burrowing 60 miles underground."
     (("Scratch" "Normal")
      ("Growl" "Normal")
      ("Dig" "Ground")
      ("Sand-Attack" "Normal")
      ("Slash" "Normal" 35)
      ("Earthquake" "Ground" 47)))
    ("Meowth"
     ("Normal")
     (1 4)
     9
     ("Persian" 28)
     "Adores circular objects. Wanders the streets on a nightly basis to look for dropped loose change."
     (("Scratch" "Normal")
      ("Growl" "Normal")
      ("Bite" "Normal" 12)
      ("Pay Day" "Normal" 17)
      ("Screech" "Normal" 24)
      ("Fury Swipes" "Normal" 33)
      ("Slash" "Normal" 44)))
    ("Persian"
     ("Normal")
     (3 3)
     71
     ()
     "Although its fur has many admirers, it is tough to raise as a pet because of its fickle meanness."
     (("Scratch" "Normal")
      ("Growl" "Normal")
      ("Bite" "Normal")
      ("Pay Day" "Normal")
      ("Screech" "Normal")
      ("Fury Swipes" "Normal" 37)
      ("Slash" "Normal" 51)))
    ("Psyduck"
     ("Water")
     (2 7)
     43
     ("Golduck" 33)
     "While lulling its enemies with its vacant look, this wily Pokemon will use psychokinetic powers."
     (("Scratch" "Normal")
      ("Tail Whip" "Normal" 28)
      ("Disable" "Normal" 31)
      ("Confusion" "Psychic" 36)
      ("Fury Swipes" "Normal" 43)
      ("Hydro Pump" "Water" 52)))
    ("Golduck"
     ("Water")
     (5 7)
     169
     ()
     "Often seen swimming elegantly by lake shores. It is often mistaken for the Japanese monster, Kappa."
     (("Scratch" "Normal")
      ("Tail Whip" "Normal")
      ("Disable" "Normal")
      ("Confusion" "Psychic" 39)
      ("Fury Swipes" "Normal" 48)
      ("Hydro Pump" "Water" 59)))
    ("Mankey"
     ("Fighting")
     (1 8)
     62
     ("Primeape" 28)
     "Extremely quick to anger. It could be docile one moment then thrashing away the next instant."
     (("Scratch" "Normal")
      ("Leer" "Normal")
      ("Karate Chop" "Normal" 15)
      ("Fury Swipes" "Normal" 21)
      ("Focus Energy" "Normal" 27)
      ("Seismic Toss" "Fighting" 33)
      ("Thrash" "Normal" 39)))
    ("Primeape"
     ("Fighting")
     (3 3)
     71
     ()
     "Always furious and tenacious to boot. It will not abandon chasing its quarry until it is caught."
     (("Scratch" "Normal")
      ("Leer" "Normal")
      ("Karate Chop" "Normal")
      ("Fury Swipes" "Normal")
      ("Focus Energy" "Normal")
      ("Seismic Toss" "Fighting" 37)
      ("Thrash" "Normal" 46)))
    ("Growlithe"
     ("Fire")
     (2 4)
     42
     ("Arcanine" "Fire Stone")
     "Very protective of its territory. It will bark and bite to repel intruders from its space."
     (("Bite" "Normal")
      ("Roar" "Normal")
      ("Ember" "Fire" 18)
      ("Leer" "Normal" 23)
      ("Take Down" "Normal" 30)
      ("Agility" "Psychic" 39)
      ("Flamethrower" "Fire" 50)))
    ("Arcanine"
     ("Fire")
     (6 3)
     342
     ()
     "A Pokemon that has been admired since the past for its beauty. It runs agilely as if on wings."
     ())
    ("Poliwag"
     ("Water")
     (2 0)
     27
     ("Poliwhirl" 25)
     "Its newly grown legs prevent it from running. It appears to prefer swimming than trying to stand."
     (("Bubble" "Water")
      ("Hypnosis" "Psychic" 16)
      ("Water Gun" "Water" 19)
      ("Doubleslap" "Normal" 25)
      ("Body Slam" "Normal" 31)
      ("Amnesia" "Psychic" 38)
      ("Hydro Pump" "Water" 45)))
    ("Poliwhirl"
     ("Water")
     (3 3)
     44
     ("Poliwrath" "Water Stone")
     "Capable of living in or out of water. When out of water, it sweats to keep its body slimy."
     (("Bubble" "Water")
      ("Hypnosis" "Psychic")
      ("Water Gun" "Water")
      ("Doubleslap" "Normal" 26)
      ("Body Slam" "Normal" 33)
      ("Amnesia" "Psychic" 41)
      ("Hydro Pump" "Water" 49)))
    ("Poliwrath"
     ("Water")
     (4 3)
     119
     ()
     "An adept swimmer at both the front crawl and breast stroke. Easily overtakes the best human swimmers."
     ())
    ("Abra"
     ("Psychic")
     (2 11)
     43
     ("Kadabra" 16)
     "Using its ability to read minds, it will identify impending danger and teleport to safety."
     (("Teleport" "Psychic")))
    ("Kadabra"
     ("Psychic")
     (4 3)
     125
     ("Alakazam" "Trade")
     "It emits special alpha waves from its body that induce headaches just by being close by."
     (("Teleport" "Psychic")
      ("Kinesis" "Psychic")
      ("Confusion" "Psychic" 16)
      ("Disable" "Normal" 20)
      ("Psybeam" "Psychic" 27)
      ("Recover" "Normal" 31)
      ("Psychic" "Psychic" 38)
      ("Reflect" "Psychic" 42)))
    ("Alakazam"
     ("Psychic")
     (4 11)
     106
     ()
     "Its brain can outperform a supercomputer. Its intelligence quotient is said to be 5,000."
     (("Teleport" "Psychic")
      ("Kinesis" "Psychic")))
    ("Machop"
     ("Fighting")
     (2 7)
     43
     ("Machoke" 28)
     "Loves to build its muscles. It trains in all styles of martial arts to become even stronger."
     (("Karate Chop" "Normal")
      ("Low Kick" "Fighting" 20)
      ("Leer" "Normal" 25)
      ("Focus Energy" "Normal" 32)
      ("Seismic Toss" "Fighting" 39)
      ("Submission" "Fighting" 46)))
    ("Machoke"
     ("Fighting")
     (4 11)
     155
     ("Machamp" "Trade")
     "Its muscular body is so powerful, it must wear a power save belt to be able to regulate its motions."
     (("Karate Chop" "Normal")
      ("Low Kick" "Fighting")
      ("Leer" "Normal")
      ("Focus Energy" "Normal" 36)
      ("Seismic Toss" "Fighting" 44)
      ("Submission" "Fighting" 52)))
    ("Machamp"
     ("Fighting")
     (5 3)
     287
     ()
     "Using its heavy muscles, it throws powerful punches that can send the victim clear over the horizon."
     (("Karate Chop" "Normal")
      ("Low Kick" "Fighting")
      ("Leer" "Normal")
      ("Focus Energy" "Normal" 36)
      ("Seismic Toss" "Fighting" 44)
      ("Submission" "Fighting" 52)))
    ("Bellsprout"
     ("Grass" "Poison")
     (2 4)
     9
     ("Weepinbell" 21)
     "A carnivorous Pokemon that traps and eats bugs. It uses its root feet to soak up needed moisture."
     (("Vine Whip" "Grass")
      ("Growth" "Normal")
      ("Wrap" "Normal" 13)
      ("Poison Powder" "Poison" 15)
      ("Sleep Powder" "Grass" 18)
      ("Stun Spore" "Grass" 21)
      ("Acid" "Poison" 26)
      ("Razor Leaf" "Grass" 33)
      ("Slam" "Normal" 42)))
    ("Weepinbell"
     ("Grass" "Poison")
     (3 3)
     14
     ("Victreebel" "Leaf Stone")
     "It spits out Poison Powder to immobilize the enemy and then finishes it with a spray of Acid."
     (("Vine Whip" "Grass")
      ("Growth" "Normal")
      ("Wrap" "Normal")
      ("Poison Powder" "Poison")
      ("Sleep Powder" "Grass")
      ("Stun Spore" "Grass" 23)
      ("Acid" "Poison" 29)
      ("Razor Leaf" "Grass" 38)
      ("Slam" "Normal" 49)))
    ("Victreebel"
     ("Grass" "Poison")
     (5 7)
     34
     ()
     "Said to live in huge colonies deep in jungles, although no one has ever returned from there."
     ())
    ("Tentacool"
     ("Water" "Poison")
     (2 11)
     100
     ("Tentacruel" 30)
     "Drifts in shallow seas. Anglers who hook them by accident are often punished by its stinging acid."
     (("Acid" "Poison")
      ("Supersonic" "Normal" 7)
      ("Wrap" "Normal" 13)
      ("Poison Sting" "Poison" 18)
      ("Water Gun" "Water" 22)
      ("Constrict" "Normal" 27)
      ("Barrier" "Psychic" 33)
      ("Screech" "Normal" 40)
      ("Hydro Pump" "Water" 48)))
    ("Tentacruel"
     ("Water" "Poison")
     (5 3)
     121
     ()
     "The tentacles are normally kept short. On hunts, they are extended to ensnare and immobilize prey."
     (("Acid" "Poison")
      ("Supersonic" "Normal")
      ("Wrap" "Normal")
      ("Poison Sting" "Poison")
      ("Water Gun" "Water")
      ("Constrict" "Normal")
      ("Barrier" "Psychic" 35)
      ("Screech" "Normal" 43)
      ("Hydro Pump" "Water" 50)))
    ("Geodude"
     ("Rock" "Ground")
     (1 4)
     44
     ("Graveler" 25)
     "Found in fields and mountains. Mistaking them for boulders, people often step or trip on them."
     (("Tackle" "Normal")
      ("Defense Curl" "Normal" 11)
      ("Rock Throw" "Rock" 16)
      ("Self Destruct" "Normal" 21)
      ("Harden" "Normal" 26)
      ("Earthquake" "Ground" 31)
      ("Explosion" "Normal" 36)))
    ("Graveler"
     ("Rock" "Ground")
     (3 3)
     232
     ("Golem" "Trade")
     "Rolls down slopes to move. It rolls over any obstacle without slowing or changing its direction."
     (("Tackle" "Normal")
      ("Defense Curl" "Normal")
      ("Rock Throw" "Rock")
      ("Self Destruct" "Normal")
      ("Harden" "Normal" 29)
      ("Earthquake" "Ground" 36)
      ("Explosion" "Normal" 43)))
    ("Golem"
     ("Rock" "Ground")
     (4 7)
     662
     ()
     "Its boulder-like body is extremely hard. It can easily withstand dynamite blasts without damage."
     (("Tackle" "Normal")
      ("Defense Curl" "Normal")
      ("Rock Throw" "Rock")
      ("Self Destruct" "Normal")
      ("Harden" "Normal" 29)
      ("Earthquake" "Ground" 36)
      ("Explosion" "Normal" 43)))
    ("Ponyta"
     ("Fire")
     (3 3)
     66
     ("Rapidash" 40)
     "Its hooves are 10 times harder than diamonds. It can trample anything completely flat in little time."
     (("Ember" "Fire")
      ("Tail Whip" "Normal" 30)
      ("Stomp" "Normal" 32)
      ("Growl" "Normal" 35)
      ("Fire Spin" "Fire" 39)
      ("Take Down" "Normal" 43)
      ("Agility" "Psychic" 48)))
    ("Rapidash"
     ("Fire")
     (5 7)
     209
     ()
     "Very competitive, this Pokemon will chase anything that moves fast in the hopes of racing it."
     (("Ember" "Fire")
      ("Tail Whip" "Normal")
      ("Stomp" "Normal")
      ("Growl" "Normal")
      ("Fire Spin" "Fire")
      ("Take Down" "Normal" 47)
      ("Agility" "Psychic" 55)))
    ("Slowpoke"
     ("Water" "Psychic")
     (3 11)
     79
     ("Slowbro" 37)
     "Incredibly slow and dopey. It takes 5 seconds for it to feel pain when under attack."
     (("Confusion" "Psychic")
      ("Disable" "Normal" 18)
      ("Head Butt" "Normal" 22)
      ("Growl" "Normal" 27)
      ("Water Gun" "Water" 33)
      ("Amnesia" "Psychic" 40)
      ("Psychic" "Psychic" 48)))
    ("Slowbro"
     ("Water" "Psychic")
     (5 3)
     173
     ()
     "The Shellder that is latched onto Slowpoke's tail is said to feed on the host's left over scraps."
     (("Confusion" "Psychic")
      ("Disable" "Normal")
      ("Head Butt" "Normal")
      ("Growl" "Normal" 27)
      ("Water Gun" "Water" 33)
      ("Withdraw" "Water" 37)
      ("Amnesia" "Psychic" 44)
      ("Psychic" "Psychic" 55)))
    ("Magnemite"
     ("Electric")
     (1 0)
     13
     ("Magneton" 30)
     "Uses anti-gravity to stay suspended. Appears without warning and uses Thunder Wave and similar moves."
     (("Tackle" "Normal")
      ("Sonicboom" "Normal")
      ("Thundershock" "Electric" 25)
      ("Supersonic" "Normal" 29)
      ("Thunder Wave" "Electric" 35)
      ("Swift" "Normal" 41)
      ("Screech" "Normal" 47)))
    ("Magneton"
     ("Electric")
     (3 3)
     132
     ()
     "Formed by several Magnemites linked together. They frequently appear when sunspots flare up."
     (("Tackle" "Normal")
      ("Sonicboom" "Normal")
      ("Thundershock" "Electric")
      ("Supersonic" "Normal")
      ("Thunder Wave" "Electric" 38)
      ("Swift" "Normal" 46)
      ("Screech" "Normal" 54)))
    ("Farfetch'd"
     ("Normal" "Flying")
     (2 7)
     33
     ()
     "The sprig of green onions it holds is its weapon. It is used much like a metal sword."
     (("Peck" "Flying")
      ("Sand-Attack" "Normal")
      ("Leer" "Normal" 7)
      ("Fury Attack" "Normal" 15)
      ("Swords Dance" "Normal" 23)
      ("Agility" "Psychic" 31)
      ("Slash" "Normal" 39)))
    ("Doduo"
     ("Normal" "Flying")
     (4 7)
     86
     ("Dodrio" 31)
     "A bird that makes up for its poor flying with its fast foot speed. Leaves giant footprints."
     (("Peck" "Flying")
      ("Growl" "Normal" 20)
      ("Fury Attack" "Normal" 24)
      ("Drill Peck" "Flying" 30)
      ("Rage" "Normal" 36)
      ("Tri Attack" "Normal" 40)
      ("Agility" "Psychic" 44)))
    ("Dodrio"
     ("Normal" "Flying")
     (5 11)
     188
     ()
     "Uses its three brains to execute complex plans. While two heads sleep, one head stays awake."
     (("Peck" "Flying")
      ("Growl" "Normal")
      ("Fury Attack" "Normal")
      ("Drill Peck" "Flying")
      ("Rage" "Normal" 39)
      ("Tri Attack" "Normal" 45)
      ("Agility" "Psychic" 51)))
    ("Seel"
     ("Water")
     (3 7)
     198
     ("Dewgong" 34)
     "The protruding horn on its head is very hard. It is used for bashing through thick ice."
     (("Head Butt" "Normal")
      ("Growl" "Normal" 30)
      ("Aurora Beam" "Ice" 35)
      ("Rest" "Psychic" 40)
      ("Take Down" "Normal" 45)
      ("Ice Beam" "Ice" 50)))
    ("Dewgong"
     ("Water" "Ice")
     (5 7)
     265
     ()
     "Stores thermal energy in its body. Swims at a steady 8 knots even in intensely cold waters."
     (("Head Butt" "Normal")
      ("Growl" "Normal" 30)
      ("Aurora Beam" "Ice" 35)
      ("Rest" "Psychic" 44)
      ("Take Down" "Normal" 50)
      ("Ice Beam" "Ice" 56)))
    ("Grimer"
     ("Poison")
     (2 11)
     66
     ("Grimer" 38)
     "Appears in filthy areas, Thrives by sucking up polluted sludge that is pumped out of factories."
     (("Pound" "Normal")
      ("Disable" "Normal")
      ("Poison Gas" "Poison" 30)
      ("Minimize" "Normal" 33)
      ("Sludge" "Poison" 37)
      ("Harden" "Normal" 42)
      ("Screech" "Normal" 48)
      ("Acid Armor" "Poison" 55)))
    ("Muk"
     ("Poison")
     (3 11)
     66
     ()
     "Thickly covered with a filthy, vile sludge. It is so toxic, even its footprints contain poison."
     (("Pound" "Normal")
      ("Disable" "Normal")
      ("Poison Gas" "Poison")
      ("Minimize" "Normal")
      ("Sludge" "Poison")
      ("Harden" "Normal" 45)
      ("Screech" "Normal" 53)
      ("Acid Armor" "Poison" 60)))
    ("Shellder"
     ("Water")
     (1 0)
     9
     ("Cloyster" "Water Stone")
     "Its hard shell repels any kind of attack. It is vulnerable only when its shell is open."
     (("Tackle" "Normal")
      ("Withdraw" "Water")
      ("Supersonic" "Normal" 18)
      ("Clamp" "Water" 23)
      ("Aurora Beam" "Ice" 30)
      ("Leer" "Normal" 39)
      ("Ice Beam" "Ice" 50)))
    ("Cloyster"
     ("Water" "Ice")
     (4 11)
     292
     ()
     "When attacked, it launches horns in quick volleys. Its innards have never been seen."
     (("Tackle" "Normal")
      ("Withdraw" "Water")
      ("Spike Cannon" "Normal" 50)))
    ("Gastly"
     ("Ghost" "Poison")
     (4 3)
     0.2
     ("Haunter" 25)
     "Almost invisible, this gaseous Pokemon cloaks the target and puts it to sleep without notice."
     (("Lick" "Ghost")
      ("Confuse Ray" "Ghost")
      ("Night Shade" "Ghost")
      ("Hypnosis" "Psychic" 27)
      ("Dream Eater" "Psychic" 35)))
    ("Haunter"
     ("Ghost" "Poison")
     (5 3)
     0.2
     ("Gengar" "Trade")
     "Because of its ability to slip through block walls, it is said to be from another dimension."
     (("Lick" "Ghost")
      ("Confuse Ray" "Ghost")
      ("Night Shade" "Ghost")
      ("Hypnosis" "Psychic" 29)
      ("Dream Eater" "Psychic" 38)))
    ("Gengar"
     ("Ghost" "Poison")
     (4 11)
     89
     ()
     "Under a full moon, this Pokemon likes to mimic the shadows of people and laugh at their fright."
     (("Lick" "Ghost")
      ("Confuse Ray" "Ghost")
      ("Night Shade" "Ghost")
      ("Hypnosis" "Psychic" 29)
      ("Dream Eater" "Psychic" 38)))
    ("Onix"
     ("Rock" "Ground")
     (28 10)
     463
     ()
     "As it grows, the stone portions of its body harden to become similar to a diamond, but colored black."
     (("Tackle" "Normal")
      ("Screech" "Normal")
      ("Bind" "Normal" 15)
      ("Rock Throw" "Rock" 19)
      ("Rage" "Normal" 25)
      ("Slam" "Normal" 33)
      ("Harden" "Normal" 43)))
    ("Drowzee"
     ("Psychic")
     (3 3)
     71
     ("Hypno" 26)
     "Puts enemies to sleep then eats their dreams. Occasionally gets sick from eating bad dreams."
     (("Pound" "Normal")
      ("Hypnosis" "Psychic")
      ("Disable" "Normal" 12)
      ("Confusion" "Psychic" 17)
      ("Head Butt" "Normal" 24)
      ("Poison Gas" "Poison" 29)
      ("Psychic" "Psychic" 32)
      ("Meditate" "Psychic" 37)))
    ("Hypno"
     ("Psychic")
     (5 3)
     167
     ()
     "When it locks eyes with an enemy, it will use a mix of PSI moves such as Hypnosis and Confusion."
     (("Pound" "Normal")
      ("Hypnosis" "Psychic")
      ("Disable" "Normal")
      ("Confusion" "Psychic")
      ("Head Butt" "Normal")
      ("Poison Gas" "Poison" 33)
      ("Psychic" "Psychic" 37)
      ("Meditate" "Psychic" 43)))
    ("Krabby"
     ("Water")
     (1 4)
     14
     ("Kingler" 28)
     "Its pincers are not only powerful weapons, they are used for balance when walking sideways."
     (("Bubble" "Water")
      ("Leer" "Normal")
      ("Vicegrip" "Normal" 20)
      ("Guillotine" "Normal" 25)
      ("Stomp" "Normal" 30)
      ("Crab Hammer" "Water" 35)
      ("Harden" "Normal" 40)))
    ("Kingler"
     ("Water")
     (4 3)
     132
     ()
     "The large pincer has 10000 hp of crushing power. However, its huge size makes it unwieldy to use."
     (("Bubble" "Water")
      ("Leer" "Normal")
      ("Vicegrip" "Normal")
      ("Guillotine" "Normal" 25)
      ("Stomp" "Normal" 34)
      ("Crab Hammer" "Water" 42)
      ("Harden" "Normal" 49)))
    ("Voltorb"
     ("Electric")
     (1 8)
     23
     ("Electrode" 30)
     "Usually found in power plants. Easily mistaken for a Poke Ball, they have zapped many people."
     (("Tackle" "Normal")
      ("Screech" "Normal")
      ("Sonicboom" "Normal" 17)
      ("Self Destruct" "Normal" 22)
      ("Light Screen" "Psychic" 29)
      ("Swift" "Normal" 36)
      ("Explosion" "Normal" 43)))
    ("Electrode"
     ("Electric")
     (3 11)
     147
     ()
     "It stores electric energy under very high pressure. It often explodes with little or no provocation."
     (("Tackle" "Normal")
      ("Screech" "Normal")
      ("Sonicboom" "Normal" 17)
      ("Self Destruct" "Normal" 22)
      ("Light Screen" "Psychic" 29)
      ("Swift" "Normal" 40)
      ("Explosion" "Normal" 50)))
    ("Exeggcute"
     ("Grass" "Psychic")
     (1 4)
     6
     ("Exeggutor" "Leaf Stone")
     "Often mistaken for eggs. When disturbed, they quickly gather and attack in swarms."
     (("Barrage" "Normal")
      ("Hypnosis" "Psychic")
      ("Reflect" "Psychic" 25)
      ("Leech Seed" "Grass" 28)
      ("Stun Spore" "Grass" 32)
      ("Poison Powder" "Poison" 37)
      ("Solar Beam" "Grass" 42)
      ("Sleep Powder" "Grass" 48)))
    ("Exeggutor"
     ("Grass" "Psychic")
     (6 7)
     265
     ()
     "Legend has it that on rare occasions, one of its heads will drop off and continue on as an Exeggcute."
     (("Barrage" "Normal")
      ("Hypnosis" "Psychic")
      ("Stomp" "Normal" 28)))
    ("Cubone"
     ("Ground")
     (1 4)
     14
     ("Marowak" 28)
     "Because it never removes its skull helmet, no one has even seen this Pokemon's real face."
     (("Growl" "Normal")
      ("Bone Club" "Ground")
      ("Leer" "Normal" 25)
      ("Focus Energy" "Normal" 31)
      ("Thrash" "Normal" 38)
      ("Bonemerang" "Ground" 43)
      ("Rage" "Normal" 46)))
    ("Marowak"
     ("Ground")
     (3 3)
     99
     ()
     "The bone it holds is its key weapon. It throws the bone skillfully like a boomerang to KO targets."
     (("Growl" "Normal")
      ("Bone Club" "Ground")
      ("Leer" "Normal")
      ("Focus Energy" "Normal" 33)
      ("Thrash" "Normal" 41)
      ("Bonemerang" "Ground" 48)
      ("Rage" "Normal" 55)))
    ("Hitmonlee"
     ("Fighting")
     (4 11)
     110
     ()
     "When in a hurry, its legs lengthen progressively. It runs smoothly with extra long, loping strides."
     (("Double Kick" "Normal")
      ("Meditate" "Psychic")
      ("Rolling Kick" "Fighting" 33)
      ("Jump Kick" "Fighting" 38)
      ("Focus Energy" "Normal" 43)
      ("Hi Jump Kick" "Fighting" 48)
      ("Mega Kick" "Normal" 53)))
    ("Hitmonchan"
     ("Fighting")
     (4 7)
     111
     ()
     "While apparently doing nothing, it fires punches in lightning fast volleys that are impossible to see."
     (("Comet Punch" "Normal")
      ("Agility" "Psychic")
      ("Fire Punch" "Fire" 33)
      ("Ice Punch" "Ice" 38)
      ("Thunder Punch" "Electric" 43)
      ("Mega Punch" "Normal" 48)
      ("Counter" "Fighting" 53)))
    ("Lickitung"
     ("Normal")
     (3 11)
     144
     ()
     "Its tongue can be extended like a chameleon's. It leaves a tingling sensation when it licks enemies."
     (("Wrap" "Normal")
      ("Supersonic" "Normal")
      ("Stomp" "Normal")
      ("Disable" "Normal")
      ("Defense Curl" "Normal")
      ("Slam" "Normal" 31)
      ("Screech" "Normal" 39)))
    ("Koffing"
     ("Poison")
     (2 0)
     2
     ("Weezing" 35)
     "Because it stores several kinds of toxic gases in it body, it is prone to explode without warning."
     (("Tackle" "Normal")
      ("Smog" "Poison")
      ("Sludge" "Poison" 32)
      ("Smoke Screen" "Normal" 37)
      ("Self Destruct" "Normal" 40)
      ("Haze" "Ice" 45)
      ("Explosion" "Normal" 48)))
    ("Weezing"
     ("Poison")
     (3 11)
     21
     ()
     "Where two kinds of poison gases meet, 2 Koffings can fuse into a Weezing over many years."
     (("Tackle" "Normal")
      ("Smog" "Poison")
      ("Sludge" "Poison")
      ("Smoke Screen" "Normal" 39)
      ("Self Destruct" "Normal" 43)
      ("Haze" "Ice" 49)
      ("Explosion" "Normal" 53)))
    ("Rhyhorn"
     ("Ground" "Rock")
     (3 3)
     254
     ("Rhydon" 42)
     "Its massive bones are 1000 times harder than human bones. It can easily knock a trailer flying."
     (("Horn Attack" "Normal")
      ("Stomp" "Normal" 30)
      ("Tail Whip" "Normal" 35)
      ("Fury Attack" "Normal" 40)
      ("Horn Drill" "Normal" 45)
      ("Leer" "Normal" 50)
      ("Take Down" "Normal" 55)))
    ("Rhydon"
     ("Ground" "Rock")
     (6 3)
     265
     ()
     "Protected by an armor-like hide, it is capable of living in molten lava of 3,600 degrees."
     (("Horn Attack" "Normal")
      ("Stomp" "Normal")
      ("Tail Whip" "Normal")
      ("Fury Attack" "Normal")
      ("Horn Drill" "Normal" 48)
      ("Leer" "Normal" 55)
      ("Take Down" "Normal" 64)))
    ("Chansey"
     ("Normal")
     (3 7)
     76
     ()
     "A rare and elusive Pokemon that is said to bring happiness to those who manage to get it."
     (("Pound" "Normal")
      ("Tail Whip" "Normal")
      ("Doubleslap" "Normal")
      ("Sing" "Normal" 24)
      ("Growl" "Normal" 30)
      ("Minimize" "Normal" 38)
      ("Defense Curl" "Normal" 44)
      ("Light Screen" "Psychic" 48)
      ("Double-Edge" "Normal" 54)))
    ("Tangela"
     ("Grass")
     (3 3)
     77
     ()
     "The whole body is swathed with wide vines that are similar to seaweed. Its vines shake as it walks."
     (("Constrict" "Normal")
      ("Bind" "Normal")
      ("Absorb" "Grass" 29)
      ("Poison Powder" "Poison" 32)
      ("Stun Spore" "Grass" 36)
      ("Sleep Powder" "Grass" 39)))
    ("Kangaskhan"
     ("Normal")
     (7 3)
     176
     ()
     "The infant rarely ventures out of its mother's protective pouch until it is 3 years old."
     (("Comet Punch" "Normal")
      ("Rage" "Normal")
      ("Bite" "Normal" 26)
      ("Tail Whip" "Normal" 31)
      ("Mega Punch" "Normal" 36)
      ("Leer" "Normal" 41)
      ("Dizzy Punch" "Normal" 46)))
    ("Horsea"
     ("Water")
     (1 4)
     18
     ("Seadra" 32)
     "Known to shoot down flying bugs with precision blasts of ink from the surface of the water."
     (("Bubble" "Water")
      ("Smoke Screen" "Normal" 19)
      ("Leer" "Normal" 24)
      ("Water Gun" "Water" 30)
      ("Agility" "Psychic" 37)
      ("Hydro Pump" "Water" 45)))
    ("Seadra"
     ("Water")
     (3 11)
     55
     ()
     "Capable of swimming backwards by rapidly flapping its wing- like pectoral fins and stout tail."
     (("Bubble" "Water")
      ("Smoke Screen" "Normal" 19)
      ("Leer" "Normal" 24)
      ("Water Gun" "Water" 30)
      ("Agility" "Psychic" 41)
      ("Hydro Pump" "Water" 52)))
    ("Goldeen"
     ("Water")
     (2 0)
     33
     ("Seaking" 33)
     "Its tail fin billow like an elegant ballroom dress, giving it the nickname of the Water Queen."
     (("Peck" "Flying")
      ("Tail Whip" "Normal")
      ("Supersonic" "Normal" 19)
      ("Horn Attack" "Normal" 24)
      ("Fury Attack" "Normal" 30)
      ("Waterfall" "Water" 37)
      ("Horn Drill" "Normal" 45)
      ("Agility" "Psychic" 54)))
    ("Seaking"
     ("Water")
     (4 3)
     86
     ()
     "In the autumn spawning season, they can be seen swimming powerfully up rivers and creeks."
     (("Peck" "Flying")
      ("Tail Whip" "Normal")
      ("Supersonic" "Normal")
      ("Horn Attack" "Normal" 24)
      ("Fury Attack" "Normal" 30)
      ("Waterfall" "Water" 39)
      ("Horn Drill" "Normal" 48)
      ("Agility" "Psychic" 54)))
    ("Staryu"
     ("Water")
     (2 7)
     76
     ("Starmie" "Water Stone")
     "An enigmatic Pokemon that can effortlessly regenerate any appendage it loses in battle."
     (("Tackle" "Normal")
      ("Water Gun" "Water" 17)
      ("Harden" "Normal" 22)
      ("Recover" "Normal" 27)
      ("Swift" "Normal" 32)
      ("Minimize" "Normal" 37)
      ("Light Screen" "Psychic" 42)
      ("Hydro Pump" "Water" 47)))
    ("Starmie"
     ("Water")
     (3 7)
     176
     ()
     "Its central core glows with the seven colors of the rainbow. Some value the core as a gem."
     ())
    ("Mr. Mime"
     ("Psychic")
     (4 3)
     120
     ()
     "If interrupted while it is miming, it will slap around the offender with its broad hands."
     (("Barrier" "Psychic")
      ("Confusion" "Psychic" 15)
      ("Light Screen" "Psychic" 23)
      ("Doubleslap" "Normal" 31)
      ("Meditate" "Psychic" 39)
      ("Substitute" "Normal" 47)))
    ("Scyther"
     ("Bug" "Flying")
     (4 11)
     123
     ()
     "With ninja-like agility and speed, it can create the illusion that there is more than one."
     (("Quick Attack" "Normal")
      ("Leer" "Normal")
      ("Focus Energy" "Normal")
      ("Double Team" "Normal" 24)
      ("Slash" "Normal" 29)
      ("Swords Dance" "Normal" 35)
      ("Agility" "Psychic" 42)))
    ("Jynx"
     ("Ice" "Psychic")
     (4 7)
     90
     ()
     "It seductively wiggles its hips as it walks. It can cause people to dance in unison with it."
     (("Pound" "Normal")
      ("Lovely Kiss" "Normal")
      ("Lick" "Ghost")
      ("Double Slap" "Normal")
      ("Ice Punch" "Ice" 31)
      ("Body Slam" "Normal" 39)
      ("Thrash" "Normal" 47)
      ("Blizzard" "Ice" 58)))
    ("Electabuzz"
     ("Electric")
     (3 7)
     66
     ()
     "Normally found near power plants, they can wander away and cause major blackouts in cities."
     (("Quick Attack" "Normal")
      ("Leer" "Normal")
      ("Thundershock" "Electric" 34)
      ("Screech" "Normal" 37)
      ("Thunder Punch" "Electric" 42)
      ("Light Screen" "Psychic" 49)
      ("Thunder" "Electric" 54)))
    ("Magmar"
     ("Fire")
     (4 3)
     98
     ()
     "Its body always burns with an orange glow that enables it to hide perfectly among flames."
     (("Ember" "Fire")
      ("Leer" "Normal" 36)
      ("Confuse Ray" "Ghost" 39)
      ("Fire Punch" "Fire" 43)
      ("Smoke Screen" "Normal" 48)
      ("Smog" "Poison" 52)
      ("Flamethrower" "Fire" 55)))
    ("Pinsir"
     ("Bug")
     (4 11)
     121
     ()
     "If it fails to crush the victim in its pincers, it will swing it around and toss it hard."
     (("Vicegrip" "Normal")
      ("Seismic Toss" "Fighting" 25)
      ("Guillotine" "Normal" 30)
      ("Focus Energy" "Normal" 36)
      ("Harden" "Normal" 43)
      ("Slash" "Normal" 49)
      ("Swords Dance" "Normal" 54)))
    ("Tauros"
     ("Normal")
     (4 7)
     195
     ()
     "When it targets an enemy, it charges furiously while whipping its body with its long tails."
     (("Tackle" "Normal")
      ("Stomp" "Normal")
      ("Tail Whip" "Normal" 28)
      ("Leer" "Normal" 35)
      ("Rage" "Normal" 44)
      ("Take Down" "Normal" 51)))
    ("Magikarp"
     ("Water")
     (2 11)
     22
     ("Gyarados" 20)
     "In the distant past, it was somewhat stronger than the horribly weak descendants that exist today."
     (("Splash" "Water")
      ("Tackle" "Normal" 15)))
    ("Gyarados"
     ("Water" "Flying")
     (21 4)
     518
     ()
     "Rarely seen in the wild. Huge and vicious, it is capable of destroying entire cities in a rage."
     (("Splash" "Water")))
    ("Lapras"
     ("Water" "Ice")
     (8 2)
     485
     ()
     "A Pokemon that has been overhunted almost to extinction. It can ferry people across the water."
     (("Water Gun" "Water")
      ("Growl" "Normal")
      ("Sing" "Normal" 16)
      ("Mist" "Ice" 20)
      ("Body Slam" "Normal" 25)
      ("Confuse Ray" "Ghost" 31)
      ("Ice Beam" "Ice" 38)
      ("Hydro Pump" "Water" 46)))
    ("Ditto"
     ("Normal")
     (1 0)
     9
     ()
     "Capable of copying an enemy's genetic code to instantly transform itself into a duplicate of the enemy."
     (("Transform" "Normal")))
    ("Eevee"
     ("Normal")
     (1 0)
     14
     ("Vaporeon, Jolteon or Flareon" "either Water, Thunder or Fire Stone")
     "Its genetic code is irregular. It may mutate if it is exposed to radiation from element Stones."
     (("Tackle" "Normal")
      ("Sand-Attack" "Normal")
      ("Quick Attack" "Normal" 27)
      ("Tail Whip" "Normal" 31)
      ("Bite" "Normal" 37)
      ("Take Down" "Normal" 45)))
    ("Vaporeon"
     ("Water")
     (3 3)
     64
     ()
     "Lives close to water. Its long tail is ridged with a fin which is often mistaken for a mermaid's."
     (("Tackle" "Normal")
      ("Sand-Attack" "Normal")
      ("Quick Attack" "Normal" 27)
      ("Water Gun" "Water" 31)
      ("Tail Whip" "Normal" 37)
      ("Bite" "Normal" 40)
      ("Acid Armor" "Poison" 42)
      ("Haze" "Ice" 44)
      ("Mist" "Ice" 48)
      ("Hydro Pump" "Water" 52)))
    ("Jolteon"
     ("Electric")
     (2 7)
     54
     ()
     "It accumulates negative ions in the atmosphere to blast out 10,000-volt lightning bolts."
     (("Tackle" "Normal")
      ("Sand-Attack" "Normal")
      ("Quick Attack" "Normal" 27)
      ("Thundershock" "Electric" 31)
      ("Tail Whip" "Normal" 37)
      ("Thunder Wave" "Electric" 40)
      ("Double Kick" "Fighting" 42)
      ("Agility" "Psychic" 44)
      ("Pin Missile" "Bug" 48)
      ("Thunder" "Electric" 52)))
    ("Flareon"
     ("Fire")
     (2 11)
     55
     ()
     "When storing thermal energy in its body, its temperature could soar to over 1,600 degrees."
     (("Tackle" "Normal")
      ("Sand-Attack" "Normal")
      ("Quick Attack" "Normal" 27)
      ("Ember" "Fire" 31)
      ("Tail Whip" "Normal" 37)
      ("Bite" "Normal" 40)
      ("Leer" "Normal" 42)
      ("Fire Spin" "Fire" 44)
      ("Rage" "Normal" 48)
      ("Flamethrower" "Fire" 54)))
    ("Porygon"
     ("Normal")
     (2 7)
     80
     ()
     "A Pokemon that consists entirely of programming code. Capable of moving freely in cyberspace."
     (("Tackle" "Normal")
      ("Sharpen" "Normal")
      ("Conversion" "Normal")
      ("Psybeam" "Psychic" 23)
      ("Recover" "Psychic" 28)
      ("Agility" "Psychic" 35)
      ("Tri Attack" "Normal" 42)))
    ("Omanyte"
     ("Rock" "Water")
     (1 4)
     17
     ("Omastar" 40)
     "Although long extinct, in rare cases, it can be genetically resurrected from fossils."
     (("Water Gun" "Water")
      ("Withdraw" "Water")
      ("Horn Attack" "Normal" 34)
      ("Leer" "Normal" 39)
      ("Spike Cannon" "Normal" 46)
      ("Hydro Pump" "Water" 53)))
    ("Omastar"
     ("Rock" "Water")
     (3 3)
     77
     ()
     "A prehistoric Pokemon that died out when its heavy shell made it impossible to catch prey."
     (("Water Gun" "Water")
      ("Withdraw" "Water")
      ("Horn Attack" "Normal")
      ("Leer" "Normal")
      ("Spike Cannon" "Normal" 44)
      ("Hydro Pump" "Water" 49)))
    ("Kabuto"
     ("Rock" "Water")
     (1 8)
     25
     ("Kabutops" 40)
     "A Pokemon that was resurrected from a fossil found in what was once the ocean floor eons ago."
     (("Scratch" "Normal")
      ("Harden" "Normal")
      ("Absorb" "Grass" 34)
      ("Slash" "Normal" 39)
      ("Leer" "Normal" 44)
      ("Hydro Pump" "Water" 49)))
    ("Kabutops"
     ("Rock" "Water")
     (4 3)
     89
     ()
     "Its sleek shape is perfect for swimming. It slashes prey with its claws and drains the body fluids."
     (("Scratch" "Normal")
      ("Harden" "Normal")
      ("Absorb" "Grass")
      ("Slash" "Normal")
      ("Leer" "Normal" 46)
      ("Hydro Pump" "Water" 53)))
    ("Aerodactyl"
     ("Rock" "Flying")
     (5 11)
     130
     ()
     "A ferocious, prehistoric Pokemon that goes for the enemy's throat with its serrated saw-like fangs."
     (("Wing Attack" "Flying")
      ("Agility" "Psychic")
      ("Supersonic" "Normal" 33)
      ("Bite" "Normal" 38)
      ("Take Down" "Normal" 45)
      ("Hyper Beam" "Normal" 54)))
    ("Snorlax"
     ("Normal")
     (6 11)
     1014
     ()
     "Very lazy. Just eats and sleeps. As its rotund bulk builds, it becomes steadily more slothful."
     (("Head Butt" "Normal")
      ("Amnesia" "Psychic")
      ("Rest" "Psychic")
      ("Body Slam" "Normal" 35)
      ("Harden" "Normal" 41)
      ("Double-Edge" "Normal" 48)
      ("Hyper Beam" "Normal" 56)))
    ("Articuno"
     ("Ice" "Flying")
     (5 7)
     122
     ()
     "A legendary bird Pokemon that is said to appear to doomed people who are lost in icy mountains."
     (("Peck" "Flying")
      ("Ice Beam" "Ice")
      ("Blizzard" "Ice" 51)
      ("Agility" "Psychic" 55)
      ("Mist" "Ice" 60)))
    ("Zapdos"
     ("Electric" "Flying")
     (5 3)
     116
     ()
     "A legendary bird Pokemon that is said to appear from clouds while dropping enormous lightning bolts."
     (("Thundershock" "Electric")
      ("Drill Peck" "Flying")
      ("Thunder" "Electric" 51)
      ("Agility" "Psychic" 55)
      ("Light Screen" "Psychic" 60)))
    ("Moltres"
     ("Fire" "Flying")
     (6 7)
     132
     ()
     "Known as the legendary bird of fire. Every flap of its wings creates a dazzling flash of flames."
     (("Peck" "Flying")
      ("Fire Spin" "Fire")
      ("Leer" "Normal" 51)
      ("Agility" "Psychic" 55)
      ("Sky Attack" "Flying" 60)))
    ("Dratini"
     ("Dragon")
     (5 11)
     7
     ("Dragonair" 30)
     "Long considered a mythical Pokemon until recently when a small colony was found living underwater."
     (("Wrap" "Normal")
      ("Leer" "Normal")
      ("Thunder Wave" "Electric")
      ("Agility" "Psychic" 20)
      ("Slam" "Normal" 30)
      ("Dragon Rage" "Dragon" 40)
      ("Hyper Beam" "Normal" 50)))
    ("Dragonair"
     ("Dragon")
     (13 1)
     36
     ("Dragonite" 55)
     "A mystical Pokemon that exudes a gentle aura. Has the ability to change climate conditions."
     (("Wrap" "Normal")
      ("Leer" "Normal")
      ("Thunder Wave" "Electric")
      ("Agility" "Psychic")
      ("Slam" "Normal" 35)
      ("Dragon Rage" "Dragon" 45)
      ("Hyper Beam" "Normal" 55)))
    ("Dragonite"
     ("Dragon" "Flying")
     (7 3)
     463
     ()
     "An extremely rarely seen marine Pokemon. Its intelligence is said to match that of humans."
     (("Wrap" "Normal")
      ("Leer" "Normal")
      ("Thunder Wave" "Electric")
      ("Agility" "Psychic")
      ("Slam" "Normal")
      ("Dragon Rage" "Dragon")
      ("Hyper Beam" "Normal" 60)))
    ("Mewtwo"
     ("Psychic")
     (6 7)
     269
     ()
     "It was created by a scientist after years of horrific gene splicing and DNA engineering experiments."
     (("Confusion" "Psychic")
      ("Disable" "Normal")
      ("Swift" "Normal")
      ("Barrier" "Psychic")
      ("Psychic" "Psychic")
      ("Recover" "Psychic")
      ("Mist" "Ice" 75)
      ("Amnesia" "Psychic" 81)))
    ("Mew"
     ("Psychic")
     (1 4)
     9
     ()
     "So rare that it is still said to be a mirage by many experts. Only a few people have seen it worldwide."
     (("Pound" "Normal")
      ("Transform" "Normal" 10)
      ("Mega Punch" "Normal" 20)
      ("Metronome" "Normal" 30)
      ("Psychic" "Psychic" 40)))))

(defun pokedex-report (n &aux (pokemon (elt *pokemon* (1- n))))
  (format nil "~a #~a: ~a" (car pokemon) n (elt pokemon 5)))

(defun pokemon-report (n &aux (pokemon (elt *pokemon* (1- n))))
  ;; TODO print attacks
  (format nil "~a #~a is a ~a type pokemon, its ~a' ~a\" high and weighs ~a lbs.~a Its moves are ~a"
	  (car pokemon)
	  n
	  (format nil "~#[~{~a~}~;~{~a~^/~}~]"
		  (elt pokemon 1))
	  (car (elt pokemon 2)) (cadr (elt pokemon 2))
	  (elt pokemon 3)
	  (if (elt pokemon 4)
	      (if (numberp (cadr (elt pokemon 4)))
		  (format nil " It evolves to ~a at level ~a!" (car (elt pokemon 4)) (cadr (elt pokemon 4)))
		  (format nil " It evolves to ~a given a ~a!" (car (elt pokemon 4)) (cadr (elt pokemon 4))))
	      "")
	  (format nil "~{~a~^,~^ ~}"
		  (mapcar (lambda (move) (apply #'format nil "~#[~;~;~a (~a)~;~a (~a) at lv. ~a~]" move)) (elt pokemon 6)))))

(defun pokemon-number (name)
  (let ((index (position name *pokemon* :test (lambda (x y) (string-equal x (car y))))))
    (if index (1+ index))))
