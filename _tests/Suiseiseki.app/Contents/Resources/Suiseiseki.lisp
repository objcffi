(progn
  (require :cl-irc)
  (require :split-sequence)

  (load "/Users/pacman/Lisp/asdf/objcffi/_tests/Suiseiseki.app/Contents/Resources/pokemon.lisp")
  (load "/Users/pacman/Lisp/asdf/objcffi/_tests/Suiseiseki.app/Contents/Resources/brainfuck.lisp")
  (load "/Users/pacman/Lisp/asdf/objcffi/_tests/Suiseiseki.app/Contents/Resources/abbrev.lisp")
  (load "/Users/pacman/Lisp/asdf/objcffi/_tests/Suiseiseki.app/Contents/Resources/clhs-lookup.lisp")
  (load "/Users/pacman/Lisp/asdf/objcffi/_tests/Suiseiseki.app/Contents/Resources/r5rs-lookup.lisp"))

(defpackage :suiseiseki
  (:use :cl :cl-irc))
(in-package :suiseiseki)

(defparameter *connection* nil)
(defparameter *commands* nil)

(defun destination (message)
  (if (string-equal (first (arguments message)) "Suiseiseki")
      (irc:source message)
      (first (irc:arguments message))))

(defmacro define-irc-command (name parameters &body body)
  `(push (cons ,(symbol-name name)
	       (lambda ,parameters
		 ,@body))
	 *commands*))

(define-irc-command !commands (message params)
  (declare (ignore params))
  (irc:privmsg *connection* (destination message)
	       (format nil "My commands are: ~{~a~^,~^ ~}." (mapcar #'string-downcase
								    (mapcar #'first *commands*)))))

(define-irc-command !random (message params)
  (let ((number (parse-integer params :junk-allowed t)))
    (unless number (setf number 6))
    (irc:privmsg *connection* (destination message)
		 (format nil "~a" (random number)))))

(defun pokemon-number-from-string (str)
  (let ((n (parse-integer str :junk-allowed t)))
    (cond ((null n) (pokemon:pokemon-number str))
	  ((not (< 0 n 152)) nil)
	  (t n))))

(define-irc-command pokedex (message params)
  (let ((n (pokemon-number-from-string params)))
    (irc:privmsg *connection* (destination message)
		 (cond ((null n) "Enter a pokemon number or name to lookup in the pokedex.")
		       (t (pokemon:pokedex-report n))))))

(define-irc-command pokemon (message params)
  (let ((n (pokemon-number-from-string params)))
    (irc:privmsg *connection* (destination message)
		 (cond ((null n) "Enter a pokemon number or name to lookup in the pokedex.")
		       (t (pokemon:pokemon-report n))))))

(define-irc-command clhs (message params)
  (irc:privmsg *connection* (destination message)
	       (or (clhs-lookup:spec-lookup params) "Nothing in the hyperspect..")))

(define-irc-command r5rs (message params)
  (irc:privmsg *connection* (destination message)
	       (or (r5rs-lookup:symbol-lookup params) "Nothing in the R5RS")))

(define-irc-command !bf (message params)
  (let ((thread
	 (sb-thread:make-thread (lambda ()
				  (handler-case
				      (irc:privmsg *connection* (destination message)
						   (with-output-to-string (s)
						     (bf:eval-brainfuck
						      (bf:string-to-brainfuck params) s)))
				    (error ()
				      (irc:privmsg *connection* (destination message)
						   "Invalid brainfuck")))))))
    (sb-thread:make-thread
     (lambda () (sleep 2)
	     (when (sb-thread:thread-alive-p thread)
	       (sb-thread:destroy-thread thread)
	       (irc:privmsg *connection* (destination message) "Thread over."))))))

(defun get-command (command message params)
  (let ((command (assoc command *commands* :test #'string-equal)))
    (if command
	(funcall (cdr command) message params))))

(defun message (message)
  (let ((split (or (position #\space (second (irc:arguments message)))
		   (length (second (irc:arguments message))))))
    (get-command (subseq (second (irc:arguments message)) 0 split)
		 message
		 (if (= split (length (second (irc:arguments message)))) ""
		     (subseq (second (irc:arguments message)) (1+ split))))))

(defun begin ()
  (setf *connection*
	(irc:connect :nickname "Suiseiseki"
		     :server "irc.freenode.net"))
  (irc:join *connection* "#lispcafe")
  (irc:start-background-message-handler *connection*)
  (add-hook *connection* 'irc::irc-privmsg-message 'message))

