(load "Suiseiseki.lisp")
(in-package :suiseiseki)
(require :cffi)
(require :objcffi)

(load "/Users/ed/bin/slime/swank-loader.lisp")
(cffi:load-foreign-library '(:framework "Cocoa"))

(begin)

(swank:create-server :port 6666)
(cffi:foreign-funcall "NSApplicationMain" :int 0 :pointer (cffi:null-pointer))
