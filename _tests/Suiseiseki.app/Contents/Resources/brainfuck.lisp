(defpackage :bf (:use :cl) (:export :string-to-brainfuck :eval-brainfuck)) (in-package :bf)

(defun string-to-brainfuck (string)
  (map 'list (lambda (character)
	       (case character
		 (#\+ '+) (#\- '-)
		 (#\< '<) (#\> '>)
		 (#\[ '[) (#\] '])
		 (#\. 'dot)
		 (otherwise (error "Invalid brainfuck"))))
       string))

(defun eval-brainfuck (code &optional (stream t) (state nil) (code-index 0) (state-index 0))
  (if (= code-index (length code))
      (return-from eval-brainfuck))
  (if (null state) (setf state (list 0)))
  (unless (null code)
    (symbol-macrolet ((state-cell (nth state-index state))
		      (code-cell (nth code-index code)))
      (case (nth code-index code)
	(dot (format stream "~c" (code-char state-cell)))
	(+ (incf state-cell))
	(- (decf state-cell))
	(> (incf state-index)
	    (if (= state-index (length state))
		(setf state (append state (list 0)))))
	(< (decf state-index)
	    (when (= state-index -1)
	      (setq state-index 0)
	      (setq state (cons 0 state))))
	([ (labels ((skip-ahead (level)
		       (incf code-index)
		       (case code-cell
			 ([ (skip-ahead (1+ level)))
			 (] (if (> 0 level) (skip-ahead (1- level))))
			 (otherwise (skip-ahead level)))))
	      (if (= 0 state-cell)
		  (skip-ahead 0))))
	(] (labels ((skip-back (level)
		       (decf code-index)
		       (case code-cell
			 (] (skip-back (1+ level)))
			 ([ (if (> 0 level)
				 (skip-back (1- level))
				 (decf code-index)))
			 (otherwise (skip-back level)))))
	      (skip-back 0))))
      (eval-brainfuck code stream state (1+ code-index) state-index))))
