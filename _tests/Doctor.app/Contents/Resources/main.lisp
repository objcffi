(require :cffi)
(require :objcffi) (import 'objcffi::self)
(require :split-sequence)
(load "/Users/ed/bin/slime/swank-loader.lisp")

(cffi:load-foreign-library '(:framework "Cocoa"))
(cffi:load-foreign-library '(:framework "WebKit"))
(cffi:load-foreign-library '(:framework "Bwana")) ;; :D

(load "abbrev.lisp")
(load "clhs-lookup.lisp")
(load "r5rs-lookup.lisp")

(objc:enable-objc-reader-syntax)

#||
@interface ManPageFormatter : NSObject {
}
+ (NSString *)manualPageFor:(NSString *)manualPage section:(NSString *)section;
+ (NSString *)unzip:(NSString *)aPath;
+ (NSString *)asciiFormatForManualPath:(NSString *)manualPagePath;
+ (NSString *)formatForHTML:(NSString *)asciiFormatedInput manual:(NSString *)manualPage;
@end
||#

(objc:define-objc-class "Doctor" "NSObject"
  ((objc:id "documentationType")
   (objc:id "display")))

(objc:define-objc-method "Doctor" - (:void "loadManPage:" objc:id sender)
  (let* ((manual-path
	  (objc:send "ManPageFormatter" "manualPageFor:section:" (objc:send sender "stringValue")
		                                                 (cffi:null-pointer)))
	 (ascii
	  (objc:send "ManPageFormatter" "asciiFormatForManualPath:" manual-path))
	 (html-code
	  (objc:send "ManPageFormatter" "formatForHTML:manual:" ascii manual-path)))
    (objc:send (objc:send (objc:ivar self "display") "mainFrame")
	       "loadHTMLString:baseURL:" html-code
	       (objc:send "NSURL" "URLWithString:" (objc:send "NSString" "stringWithUTF8String:" "")))))

(objc:define-objc-method "Doctor" - (:void "loadDocumentation:" objc:id sender)
  (let ((the-url (r5rs-lookup:symbol-lookup [[sender stringValue] UTF8String])))
    (format t "BLAH ~a~%" the-url)
    [[(objc:ivar self "display") mainFrame]
     loadRequest:[NSURLRequest requestWithURL:
			       [NSURL URLWithString:[NSString stringWithUTF8String:the-url]]]]))

(swank:create-server)
(cffi:foreign-funcall "NSApplicationMain" :int 0 :pointer (cffi:null-pointer))
