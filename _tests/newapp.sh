cp -r "Generic Application.app" "$1.app"
sed -e "s/Generic Application/$1/" "Generic Application.app/Contents/Info.plist" > "$1.app/Contents/Info.plist"
mv "$1.app/Contents/MacOS/Generic Application" "$1.app/Contents/MacOS/$1"
rm "$1.app/Contents/MacOS/sbcl"
ln -s `which sbcl` "$1.app/Contents/MacOS/sbcl"
