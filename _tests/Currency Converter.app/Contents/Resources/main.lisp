(require :cffi)
(require :objcffi)
(import 'objcffi::self) ;; this is so I can use 'self' as opposed to objc::self

;;(load "/Users/ed/bin/slime/swank-loader.lisp")

(defparameter *rate* 2) ;; You can connect in with SLIME (when the program is running) and edit this.. or any other part of the code

(cffi:load-foreign-library '(:framework "Cocoa"))

(objc:enable-objc-reader-syntax) ;; now we can use []'s, its good to be able to toggle on and off

(objc:define-objc-class "Converter" "NSObject"
  ((objc:id "dollars")
   (objc:id "pounds")))

(objc::with-ivars self (("pounds" pounds) ("dollars" dollars)) ;; can nest multiple methods in here
  (objc:define-objc-method "Converter" - (:void "performConvertion:" objc:id sender)
    [pounds setFloatValue:(* *rate* [dollars floatValue])]))

;;(swank:create-server) ;; so we can connect in an hack the running program
(cffi:foreign-funcall "NSApplicationMain" :int 0 :pointer (cffi:null-pointer)) ;; main run loop
