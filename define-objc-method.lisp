(in-package :objcffi)

;; TODO this -must- invaliadate the entry in send's hashtable of compiled functions
(defun-with-types %define-objc-method ((class :objc-class) selector-name
				      method-type imp-callback
				      &optional (instance-method t))
  ;; TODO maybe should remove the old method if one already exists?
  (let* ((method-list (cffi:foreign-alloc 'objc_method_list))
	 (method-ptr (cffi:inc-pointer
		      method-list
		      (cffi:foreign-slot-offset 'objc_method_list 'method_list)))
	 (method (cffi:mem-ref method-ptr 'objc_method)))
    (setf (cffi:foreign-slot-value method-list 'objc_method_list 'method_count) 1)
    (setf (cffi:foreign-slot-value method 'objc_method 'method_name)
	  (%sel_getUid selector-name)
	  
	  (cffi:foreign-slot-value method 'objc_method 'method_types)
	  method-type
	  
	  (cffi:foreign-slot-value method 'objc_method 'method_imp)
	  imp-callback)
    (%class_addMethods (if instance-method class (cffi:foreign-slot-value class 'objc_class 'isa))
		       method-list)))

(defun plist->alist+ (plist) ;; shouldn't this be elsewhere?
  ;; the + is because this isn't really plist->alist
  ;; Instead of turning (X 1 Y 2 Z 3) into ((X . 1) (Y . 2) (Z . 3))
  ;; it outputs ((1 X) (2 Y) (3 Z))
  (if (endp plist) nil
      (cons (list (second plist) (first plist))
	    (plist->alist+ (rest (rest plist))))))

(defmacro define-objc-method (class instance-method (return-type selector-name &rest parameters) &body body)
  (let ((callback (gensym selector-name))
	(parameters-alist (plist->alist+ parameters)))
    `(progn (cffi:defcallback ,callback ,return-type
	        ((self :pointer) (_cmd :pointer) ,@parameters-alist)
	      (declare (ignorable self _cmd))
	      ;; TODO? make bindings for class ivars?
	      (restart-case
		  (progn ,@body)
		(return () :report "Return immediatly")))
	    (%define-objc-method
	      ,class
	      ,selector-name
	      ,(encode-method-type return-type (mapcar #'second parameters-alist))
	      (cffi:callback ,callback)
	      ,(eq instance-method '-)))))
