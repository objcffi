(in-package :objcffi)

(defun ivar (object name)
  (assert (cffi:pointerp object))
  (let* ((ivar
	  (%class_getInstanceVariable
	   (cffi:foreign-slot-value object 'objc_object 'isa) name))
	 (decoded-type (decode-type (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_type))))
    (cffi:with-foreign-object (x decoded-type)
      (%object_getInstanceVariable object name x)
      (cffi:mem-ref x decoded-type))))

(defun set-ivar (object name value)
  (assert (cffi:pointerp object))
  (let* ((ivar
	  (%class_getInstanceVariable
	   (cffi:foreign-slot-value object 'objc_object 'isa) name))
	 (decoded-type (decode-type (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_type)))
	 (ivar-place (cffi:inc-pointer
		      object
		      (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_offset))))
    (setf (cffi:mem-ref ivar-place decoded-type) value)))

(defsetf ivar set-ivar)

(defmacro with-ivars (object (&rest names-and-symbols) &body body) ;; hopefully phase this out
  `(symbol-macrolet ,(mapcar (lambda (name-and-symbol)
			       (list (second name-and-symbol)
				     `(ivar ,object ,(first name-and-symbol))))
			     names-and-symbols)
     ,@body))

(defun guess-cffi-type (object) ;; used for varargs type-guessing
  (cond ((cffi:pointerp object) :pointer)
	((stringp object) :string)
	((integerp object) :int) ;; int long etc ?
	((numberp object) :double) ;; needs more checks float/double-float
	(t (cerror "Use :pointer" "No applicable CFFI type for object \"~S\" [~a]" object (type-of object))
	   :pointer)))

(let (defined-structures (compiled-messages (make-hash-table :test #'equal)))
  (defun name-objc-struct (name struct)
    (push (cons name struct) defined-structures))
  
  (defun canonicalize-type (type)
    (if (and (listp type)
	     (eq (first type) :struct))
	(let ((struct
	       (assoc (second type) defined-structures :test #'string=)))
	  (if struct (cdr struct)
	      (error "Unknown struct type \"~a\"" (second type)))) ;; should possibly define the structure at this moment
	type))
  
  (defun compile-message (function-prototype)
    (let* ((return-type (first function-prototype))
	   (parameter-types (rest function-prototype))
	   (gensyms (loop repeat (length parameter-types) collect (gensym "parameter")))
	   (types-and-gensyms (mapcan #'list parameter-types gensyms)))
      (compile nil `(lambda ,gensyms
		      (cffi:foreign-funcall ,(cond ((and (find return-type defined-structures :key #'cdr)
							 (not (find (cffi:foreign-type-size return-type) '(1 2 4 8))))
						    "objc_msgSend_stret")
						   ((find return-type '(:float :double))
						    "objc_msgSend_fpret")
						   (t "objc_msgSend"))
					    ,@types-and-gensyms
					    ,return-type)))))
  
  (defun get-compiled-message (function-prototype)
    (let ((compiled-message (gethash function-prototype compiled-messages)))
      (unless compiled-message
	(return-from get-compiled-message
	  (setf (gethash function-prototype compiled-messages) (compile-message function-prototype))))
      compiled-message))
  
  (defun-with-types send ((object name instance :objc-object)
			  (selector selector-name :objc-selector)
			  &rest parameters)
    (multiple-value-bind (method-types method)
	(get-method-types (if instance (cffi:foreign-slot-value object 'objc_object 'isa) object)
			  selector
			  instance)
      (declare (ignore method))
      (setf method-types (decode-method-types method-types))
      (let ((function-prototype (mapcar #'canonicalize-type
					(append method-types
						(mapcar #'guess-cffi-type ;; Variadic function call support
							(nthcdr (- (length method-types) 1 2)
								parameters))))))
	(apply (get-compiled-message function-prototype) object selector parameters)))))
