(in-package :objcffi)

;;; TODO
;; doesnt support arrays yet,
;; see (%list-ivars (%objc_getClass "NSConcreteFileHandle"))

;; Table based on
;; http://developer.apple.com/documentation/Cocoa/Conceptual/ObjectiveC/Articles/chapter_5_section_7.html
(defparameter *type-encodings*
  '(;; Basic CFFI types
    (:char "c")
    (:unsigned-char "C")
    (:uchar "C")
    (:short "s")
    (:unsigned-short "S")
    (:ushort "S")
    (:int "i")
    (:unsigned-int "I")
    (:uint "I")
    (:long "l")
    (:unsigned-long "L")
    (:ulong "L")
    (:long-long "q")
    (:llong "q")
    (:unsigned-long-long "Q")
    (:ullong "Q")
    (:float "f")
    (:double "d")
    (:long-double "d") ;; test this on all platforms, unknown on ppc
    (:void "v")
    (:pointer "^v")
    (:string "*")
    
    ;; Objective C types
    (id "@")
    (SEL ":")
    (BOOL "c")
    (IMP "^?")
    (Class "#")
    (:unknown "?" 4))) ;; this size may be implementation specific

(defparameter *method-encodings*
  '((#\r :const)
    (#\n :in)
    (#\N :inout)
    (#\o :out)
    (#\O :bycopy)
    (#\R :byref)
    (#\V :oneway)))

(defun-with-types get-method-types ((class class-name :objc-class)
				    (selector selector-name :objc-selector)
				    &optional instance-method)
  (let ((method (funcall (if instance-method #'%class_getInstanceMethod #'%class_getClassMethod)
			 class selector)))
    (if (cffi:null-pointer-p method)
	(error "Class \"~a\" does not have the ~:[class~;instance~] method \"~a\""
	       (if (cffi:null-pointer-p class) "<NULL>" class-name)
	       instance-method selector-name)
	(values (cffi:foreign-slot-value method 'objc_method 'method_types)
		method))))

(defun encode-struct (struct)
  (format nil "{~a}"
	  (apply #'concatenate 'string
		 (second struct) "=" (mapcar #'encode-type (cddr struct)))))

(defun encode-type (type)
  (let ((encoding (assoc type *type-encodings*)))
    (cond ((eq type :void) (second encoding))
	  ((and (listp type) (eq (first type) :struct)) (encode-struct type))
	  (t (values (string (second encoding))
		     (or (third encoding)
			 (cffi:foreign-type-size type)))))))

(defun encode-method-type (return-type parameters)
  (apply #'concatenate 'string
	 (encode-type return-type) "@:" (mapcar #'encode-type parameters)))


(defun decode-type (type-string)
  (if (char= #\^ (elt type-string 0)) :pointer
      (first (find type-string *type-encodings* :key #'second :test #'string=))))

(defun decode-struct-from-method-type (stream)
  (let ((name ())
	(types ()))
    (do ((char (read-char stream) (read-char stream)))
	((char= #\= char))
      (push char name))
    
    (when (char= (peek-char nil stream) #\")
      (read-char stream)
      (loop while (char/= #\" (read-char stream))))
    
    (labels ((read-until-end ()
	       (do ((char (read-char stream) (read-char stream)))
		   ((char= #\} char)
		    (push char types))
		 (push char types)
		 (when (char= #\{ char)
		   (read-until-end)))))
      (read-until-end))
    (list* :struct
	   (coerce (reverse name) 'string)
	   (decode-method-types (coerce (reverse types) 'string)))))

(defun decode-method-types (method-types)
  (let ((types ()))
    (with-input-from-string (s method-types)
      (do* ((token (read-char s) (read-char s nil :eof)))
	   ((eq token :eof))
	(let ((objc-name (find (string token) *type-encodings*
			       :key #'second :test #'string=)))
	  (cond (objc-name
		 (push (first objc-name) types))
		((char= token #\^)
		 (push :pointer types)
		 (read-char s nil))
		 ((char= token #\{)
		 (push (decode-struct-from-method-type s) types))
		((char= token #\")
		 (loop while (char/= #\" (read-char s))))))))
    (reverse types)))
