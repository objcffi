#include <stdio.h>
#include <objc/objc-runtime.h>
#import <objc/hashtable.h>
#include <Foundation/Foundation.h>

void showIvars(Class klass) {
  int i;
  Ivar rtIvar;
  struct objc_ivar_list* ivarList = klass->ivars;
  if (ivarList!= NULL && (ivarList->ivar_count>0)) {
    printf ("  Instance Variabes:\n");
    for ( i = 0; i < ivarList->ivar_count; ++i ) {
        rtIvar = (ivarList->ivar_list + i);
        printf ("    name: '%s'  encodedType: '%s'  offset: %d\n",
                rtIvar->ivar_name, rtIvar->ivar_type, rtIvar->ivar_offset);
    }
  }
}

int main(void) {
    NXHashTable * class_hash = objc_getClasses(); // NOT in a multi-threaded*** App.
    NXHashState state = NXInitHashState(class_hash);
    struct objc_class * klass;
    while (NXNextHashState(class_hash, &state, (void **)&klass)) {
        printf ("%s\n", klass->name);
        showIvars(klass);
        //showMethodGroups(klass, '-'); // instance methods
        //showMethodGroups(klass->isa, '+'); // class methods
    }
  return 0;
}
