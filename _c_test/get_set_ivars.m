#include <stdio.h>
#include <objc/objc-runtime.h>

#import <Cocoa/Cocoa.h>

@interface Foo : NSObject
{
  int x;
}
- (int)get;
@end

@implementation Foo
- (id)init { self = [super init]; x = 666; return self; }
- (int)get { return x; }
@end

int main(void) {
  Foo * f = [[Foo alloc] init];
  
  printf("%d\n", [f get]);
  {
    int x;
    object_getInstanceVariable(f, "x", (void*)&x);
    printf("%d\n", x);
  }
  {
    int x = 35;
    object_setInstanceVariable(f, "x", (void*)x);
  }
  printf("%d\n", [f get]);
  
  return 0;
}
