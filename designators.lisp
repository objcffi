(in-package :objcffi)

(defmethod persuade ((type (eql :objc-selector)) object)
  (if (cffi:pointerp object) (values object (%sel_getName object))
      (error "Cannot persuade object of type ~a into an objc-selector" (type-of object))))
(defmethod persuade ((type (eql :objc-selector)) (object string))
  (values (%sel_getUid object) object))


(defmethod persuade ((type (eql :objc-class)) object)
  (if (cffi:pointerp object) (values object
				     (cffi:foreign-slot-value object 'objc_class 'name))
      (error "Cannot persuade object of type ~a into an objc-class" (type-of object))))
(defmethod persuade ((type (eql :objc-class)) (object string))
  (values (%objc_getClass object) object))


(defmethod persuade ((type (eql :objc-object)) object)
  (if (cffi:pointerp object)
      (let* ((instance (%instancep object))
	     (isa-object (if instance (cffi:foreign-slot-value object 'objc_object 'isa) object)))
	(values object
		(cffi:foreign-slot-value isa-object 'objc_class 'name)
		instance))
      (error "Cannot persuade object of type ~a into an objc-object" (type-of object))))
(defmethod persuade ((type (eql :objc-object)) (object string))
  (values (%objc_getClass object) object t))
