(defpackage :objcffi
  (:use :cl :asdf :cffi :defun-with-types)
  (:nicknames :objc)
  (:import-from :defun-with-types :persuade)
  (:export :BOOL :id :Ivar :Method :Class :SEL
	   :get-method-types :encode-type :encode-method-type :decode-type :decode-method-types
	   :define-objc-class
	   :define-objc-method
	   
	   :ivar :send
	   :enable-objc-reader-syntax
	   
	   :NSRange :NSPoint :NSSize :NSRect
	   :NSMakeRange :NSMakePoint :NSMakeSize :NSMakeRect
	   :location :length :x :y :width :height :origin :size))
(in-package :objcffi)

(cffi:use-foreign-library (:framework "Cocoa")) ;; maybe should just load libobjc instead?
