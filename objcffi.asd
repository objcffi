(defsystem :objcffi
  :depends-on (:cffi :defun-with-types)
  :serial t
  :components
   ((:file "package")
;; objc-include-* are files that bind directly to the stuff in /usr/include/objc/
;; they're be mechanically generated from parsing the headers
;; (with a bit of fixing up after)
    (:file "objc-include-types")
    (:file "objc-include-constants")
    (:file "objc-include-functions")
    (:file "objc-include-utils")
    
;; Designators provides some type conversion rules for defun-with-types
;; this gives an object oriented extention to defun which lets us pass
;; strings or pointers [or other things as the API evolves] as parameters
;; to functions that require a class or selector
    (:file "designators")
    
;; The parameter and return type of a method are encoded as type encodings,
;; Provide a way to encode and decode them, as well as extract them from a Method.
    (:file "type-encodings")
    
;; Procedures to dynamically create Objective C Classes and add Methods to them
    (:file "define-objc-class")
    (:file "define-objc-method")
    
;; Provide various auxilliary methods which give an interface to useful operations
;; like message sending and ivar access, also a [] and @"foo" reader syntax.
    (:file "auxiliary")
    (:file "reader-syntax")
    
;; Implement any inline functions in the Cocoa (Foundation+AppKit) APIs
;; add any structs they define (and declare them for SEND to use)
    (:file "cocoa")))
