(in-package :objcffi)

(cffi:defctype BOOL (:boolean :char))
(cffi:defctype id :pointer)
(cffi:defctype Ivar :pointer)
(cffi:defctype Method :pointer)
(cffi:defctype Class :pointer)
(cffi:defctype SEL :pointer)
(cffi:defctype IMP :pointer)

(cffi:defcstruct objc_object
   (isa Class))

(cffi:defcstruct objc_class
   (isa Class)
   (super_class Class)
   (name :string)
   (version :long)
   (info :long)
   (instance_size :long)
   (ivars :pointer) ;; objc_ivar_list *
   (methodLists :pointer) ;; objc_method_list **
   (cache :pointer) ;; objc_cache *
   (protocols :pointer)) ;; objc_protocol_list *

(cffi:defcstruct objc_category
   (category_name :string)
   (class_name :string)
   (instance_methods :pointer) ;; objc_method_list *
   (class_methods :pointer) ;; objc_method_list *
   (protocols :pointer)) ;; objc_protocol_list *

(cffi:defcstruct objc_ivar
   (ivar_name :string)
   (ivar_type :string)
   (ivar_offset :int))

(cffi:defcstruct (objc_ivar_list :size 16) ;; check these sizes are required (and correct)
   (ivar_count :int)
   (ivar_list :pointer)) ;; "struct objc_ivar[1]"

(cffi:defcstruct objc_method
   (method_name sel)
   (method_types :string)
   (method_imp imp))

(cffi:defcstruct (objc_method_list :size 20) ;; check these sizes are required (and correct)
   (obsolete :pointer) ;; objc_method_list *
   (method_count :int)
   (method_list :pointer)) ;; "struct objc_method[1]"

(cffi:defcstruct (objc_protocol_list :size 12) ;; check these sizes are required (and correct)
   (next :pointer) ;; objc_protocol_list *
   (count :int)
   (list :pointer)) ;; "Protocol *[1]"

(cffi:defcstruct (objc_cache :size 12) ;; check these sizes are required (and correct)
   (mask :unsigned-int)
   (occupied :unsigned-int)
   (buckets :pointer)) ;; "Method[1]"
