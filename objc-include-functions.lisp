(in-package :objcffi)

(cffi:defctype BOOL (:boolean :char))
(cffi:defctype id :pointer)
(cffi:defctype Ivar :pointer)
(cffi:defctype Method :pointer)
(cffi:defctype Class :pointer)
(cffi:defctype SEL :pointer)

;; warning needs pitstop each pipe.
;; then a lot of touching up at the end
;; grep -h "OBJC_EXPORT" /usr/include/objc/objc(|-(|class|runtime)).h | grep -v ")(" | tr "\t" " " | sed -e 's/ *OBJC_EXPORT *\(.*\)/\1/' | sed -e 's/ (/(/' | sed -e 's/\(.*\) \(.*\)[ ]*(\(.*\));/(cffi:defcfun ("\2" %\2) :\1 (\3))/' -e 's/\.\.\./\&rest/' | sed -e 's/\, /\) \(/g' |  sed -e 's/(\([^ ]*\) \([^ )]*\))/(\2 \1)/g' | sed -e 's/(\([^ ]*\) \([^ )]*\))/(\2 \1)/'

(cffi:defcfun ("objc_collect_if_needed" %objc_collect_if_needed)
   :void (options :unsigned-long))

(cffi:defcfun ("objc_numberAllocated" %objc_numberAllocated)
   :unsigned-int)

(cffi:defcfun ("objc_collecting_enabled" %objc_collecting_enabled)
   BOOL)

(cffi:defcfun ("objc_allocate_object" %objc_allocate_object)
   id (cls Class) (extra :int))

(cffi:defcfun ("objc_assign_strongCast" %objc_assign_strongCast)
   id (val id) (dest :pointer)) ;; dest is type id *

(cffi:defcfun ("objc_assign_global" %objc_assign_global)
   id (val id) (dest :pointer)) ;; dest is type id *

(cffi:defcfun ("objc_assign_ivar" %objc_assign_ivar)
   id (value id) (dest id) (offset :unsigned-int))

;;(cffi:defcfun ("objc_memmove_collectable" %objc_memmove_collectable)
;;   :pointer (dst :pointer) (src :pointer) (size :int)) ;; size is type size_t

(cffi:defcfun ("objc_is_finalized" %objc_is_finalized)
   BOOL (ptr :pointer))

(cffi:defcfun ("object_setInstanceVariable" %object_setInstanceVariable)
   Ivar (id id) (name :string) (variable :pointer))

(cffi:defcfun ("object_getInstanceVariable" %object_getInstanceVariable)
   Ivar (id id) (name :string) (variable :pointer)) ;; varible is type void **

(cffi:defcfun ("class_createInstance" %class_createInstance)
   id (class Class) (idxIvars :unsigned-int))

(cffi:defcfun ("class_createInstanceFromZone" %class_createInstanceFromZone)
   id (class Class) (idxIvars :unsigned-int) (z :pointer))

(cffi:defcfun ("class_setVersion" %class_setVersion)
   :void (class Class) (version :int))

(cffi:defcfun ("class_getVersion" %class_getVersion)
   :int (class Class))

(cffi:defcfun ("class_getInstanceVariable" %class_getInstanceVariable)
   Ivar (class Class) (variable :string))

(cffi:defcfun ("class_getInstanceMethod" %class_getInstanceMethod)
   Method (class Class) (selector SEL))

(cffi:defcfun ("class_getClassMethod" %class_getClassMethod)
   Method (class Class) (selector SEL))

(cffi:defcfun ("class_addMethods" %class_addMethods)
   :void (class Class) (method-list :pointer)) ;; method-list is type struct objc_method_list *

(cffi:defcfun ("class_removeMethods" %class_removeMethods)
   :void (class Class) (method-list :pointer)) ;; method-list is type struct objc_method_list *

(cffi:defcfun ("class_poseAs" %class_poseAs)
   Class (imposter Class) (original Class))

(cffi:defcfun ("method_getNumberOfArguments" %method_getNumberOfArguments)
   :unsigned-int (method Method))

(cffi:defcfun ("method_getSizeOfArguments" %method_getSizeOfArguments)
   :unsigned-int (method Method))

(cffi:defcfun ("method_getArgumentInfo" %method_getArgumentInfo)
   :unsigned-int (m Method) (arg :int) (type :pointer) (offset :pointer)) ;; type is type const char **, offset is type int *

(cffi:defcfun ("class_nextMethodList" %class_nextMethodList)
   :pointer (class Class) (method-list :pointer)) ;; void **, return type is struct objc_method_list *

(cffi:defcfun ("objc_exception_throw" %objc_exception_throw)
   :void (exception id))

(cffi:defcfun ("objc_exception_try_enter" %objc_exception_try_enter)
   :void (localExceptionData :pointer))

(cffi:defcfun ("objc_exception_try_exit" %objc_exception_try_exit)
   :void (localExceptionData :pointer))

(cffi:defcfun ("objc_exception_extract" %objc_exception_extract)
   id (localExceptionData :pointer))

(cffi:defcfun ("objc_exception_match" %objc_exception_match)
   :int (exceptionClass Class) (exception id))

(cffi:defcfun ("objc_exception_get_functions" %objc_exception_get_functions)
   :void (table :pointer)) ;; table is type objc_exception_functions_t *

(cffi:defcfun ("objc_exception_set_functions" %objc_exception_set_functions)
   :void (table :pointer)) ;; table is type objc_exception_functions_t *

(cffi:defcfun ("objc_getClass" %objc_getClass)
   id (name :string))

(cffi:defcfun ("objc_getMetaClass" %objc_getMetaClass)
   id (name :string))

(cffi:defcfun ("objc_msgSend" %objc_msgSend)
   id (self id) (op SEL) &rest)

(cffi:defcfun ("objc_msgSendSuper" %objc_msgSendSuper)
   id (super :pointer) (op SEL) &rest) ;; super is type struct objc_super *

(cffi:defcfun ("objc_msgSend_fpret" %objc_msgSend_fpret)
   :double (self id) (op SEL) &rest)

(cffi:defcfun ("objc_msgSend_stret" %objc_msgSend_stret)
   id (self id) (op SEL) &rest)

(cffi:defcfun ("objc_msgSendSuper_stret" %objc_msgSendSuper_stret)
   id (super :pointer) (op SEL) &rest) ;; super is type struct objc_super *

(cffi:defcfun ("objc_msgSend_stret" %objc_msgSend_stret)
   :void (stretAddr :pointer) (self id) (op SEL) &rest)

(cffi:defcfun ("objc_msgSendSuper_stret" %objc_msgSendSuper_stret)
   :void (stretAddr :pointer) (super :pointer) (op SEL) &rest) ;; super is type struct objc_super *

;;(cffi:defcfun ("objc_msgSendv" %objc_msgSendv)
;;   id (self id) (op SEL) (arg_size :unsigned-int) (arg_frame marg_list))

;;(cffi:defcfun ("objc_msgSendv_stret" %objc_msgSendv_stret)
;;   :void (stretAddr :pointer) (self id) (op SEL) (arg_size :unsigned-int) (arg_frame marg_list))

;;(cffi:defcfun ("objc_msgSendv_fpret" %objc_msgSendv_fpret)
;;   :double (self id) (op SEL) (arg_size :unsigned-int) (arg_frame marg_list))

(cffi:defcfun ("objc_getClassList" %objc_getClassList)
   :int (buffer :pointer) (bufferLen :int)) ;; buffer is type Class *

(cffi:defcfun ("objc_getClasses" %objc_getClasses)
   :pointer)

(cffi:defcfun ("objc_lookUpClass" %objc_lookUpClass)
   id (name :string))

(cffi:defcfun ("objc_getRequiredClass" %objc_getRequiredClass)
   id (name :string))

(cffi:defcfun ("objc_addClass" %objc_addClass)
   :void (myClass Class))

(cffi:defcfun ("objc_setMultithreaded" %objc_setMultithreaded)
   :void (flag BOOL))

(cffi:defcfun ("objc_sync_enter" %objc_sync_enter)
   :int (obj id))

(cffi:defcfun ("objc_sync_exit" %objc_sync_exit)
   :int (obj id))

(cffi:defcfun ("objc_sync_wait" %objc_sync_wait)
   :int (obj id) (milliSecondsMaxWait :long-long))

(cffi:defcfun ("objc_sync_notify" %objc_sync_notify)
   :int (obj id))

(cffi:defcfun ("objc_sync_notifyAll" %objc_sync_notifyAll)
   :int (obj id))

(cffi:defcfun ("sel_isMapped" %sel_isMapped)
   BOOL (sel SEL))

(cffi:defcfun ("sel_getName" %sel_getName)
   :string (sel SEL))

(cffi:defcfun ("sel_getUid" %sel_getUid)
   SEL (str :string))

(cffi:defcfun ("sel_registerName" %sel_registerName)
   SEL (str :string))

(cffi:defcfun ("object_getClassName" %object_getClassName)
   :string (obj id))

(cffi:defcfun ("object_getIndexedIvars" %object_getIndexedIvars)
   :pointer (obj id))

