(in-package :objcffi)

(defun read-until-characters (stream characters &key (eof 'eof) (skip t))
  (let* ((eos nil)
	 (string (with-output-to-string (result)
		   (if skip
		       (loop for char = (read-char stream nil eof)
			  until (or (eq eof char) (find char characters :test #'char=))
			  do (write-char char result)
			  finally (setq eos char))
		       (loop for char = (peek-char nil stream)
			  until (or (eq eof char) (find char characters :test #'char=))
			  do (read-char stream nil)
			  do (write-char char result)
			  finally (setq eos char))))))
    (values string eos)))

(defun probably-objc-class-name (object name)
  (and (not (string= "" name))
       (symbolp object)
       (upper-case-p (elt name 0))))

(defun skip-whitespace (stream)
  (do ((ch (peek-char nil stream nil #\!)
	   (peek-char nil stream nil #\!)))
      ((not (find ch '(#\Space #\Tab #\Newline #\Return))))
    (read-char stream)))

(defun utf-8-string (string)
  #+sbcl (sb-ext:string-to-octets string :external-format :utf-8))

(defun enable-objc-reader-syntax ()
  ;; Makes the following assumptions
  ;; if the first expression e.g. [expression ...]
  ;; starts with either #\[ or #\(,
  ;;   then it is a compound expression so (read) it
  ;; otherwise read up to 'whitespace'
  ;;   (which is assumed to be any of (#\space #\tab #\newline #\return))
  ;; and check if it "looks" like an objc-class-name
  (set-macro-character #\@ (lambda (stream char) ;; TODO if its not a string produce a warning
			     `(send "NSString" "stringWithUTF8String:" ,(utf-8-string (read stream)))))
  (set-macro-character #\] (get-macro-character #\) nil))
  (set-macro-character #\[ (lambda (stream char)
			     (declare (ignore char))
				 (let* (object-name object
				        (selector-name "")
					(parameters nil))
				   (if (find (peek-char t stream) '(#\[ #\()) ;; this is -really- bad
				       (setf object-name ""
					     object (read stream))
				       (setf object-name (read-until-characters stream
							   '(#\space #\tab #\newline #\return)) ;; again this sucks, a lot
					     object (read-from-string object-name)))
				   (labels ((read-parameter ()
					      (skip-whitespace stream)
					      (multiple-value-bind (name end)
						  (read-until-characters stream ":]")
						(cond ((char= #\: end)
						       (setf selector-name
							     (concatenate 'string selector-name name ":"))
						       (push (read stream) parameters)
						       (read-parameter))
						      (t (setf selector-name
							       (concatenate 'string selector-name name)))))))
				     (read-parameter))
				   (setf parameters (reverse parameters))
				   (append (list 'send
						 (if (probably-objc-class-name object object-name) object-name object)
						 selector-name)
					   parameters)))))
