(in-package :objcffi)

;; Low level (dangerous utils at the level of objc-include)

(defun %instancep (objc-class)
  ;; parameter -must- be a pointer to either an objc_class or an objc_object
  ;; or this function will almost def. crash
  ;;
  ;; Is this an instance or a class?
  ;; pass in either an instance, or an object
  (assert (cffi:pointerp objc-class))
  (cls-get-info (cffi:foreign-slot-value 
		 (cffi:foreign-slot-value objc-class 'objc_object 'isa)
		 'objc_class 'info)
		CLS_CLASS))

(defun %list-ivars (objc-class)
  ;; returns an alist like ((id "ivarName") (:float "anotherIvar") ...)
  (assert (cffi:pointerp objc-class))
  (let* ((ivars (cffi:foreign-slot-value objc-class 'objc_class 'ivars)))
    (if (cffi:null-pointer-p ivars) nil
	(let* ((ivar-count (cffi:foreign-slot-value ivars 'objc_ivar_list 'ivar_count))
	       (ivar (cffi:inc-pointer ivars (cffi:foreign-slot-offset 'objc_ivar_list 'ivar_list))))
	  (loop
	     repeat ivar-count
	     collect (list (decode-type (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_type))
			   (cffi:foreign-slot-value ivar 'objc_ivar 'ivar_name))
	     do (cffi:incf-pointer ivar (cffi:foreign-type-size 'objc_ivar)))))))

;;; (%list-ivars (%objc_getClass "NSApplication"))
